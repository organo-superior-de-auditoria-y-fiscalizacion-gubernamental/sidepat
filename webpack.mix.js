const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            '@': __dirname + '/resources'
        }
    }
});

/*
 / 'globalVueStyles': Allow read scss variables in each component
 / this option only works when 'extractVueStyles' is true, only
 / include variables, functions or mixins.
 */
mix.options({
    extractVueStyles: true,
    globalVueStyles: 'resources/sass/variables.scss'
});


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/home.scss', 'public/css/app.css');
