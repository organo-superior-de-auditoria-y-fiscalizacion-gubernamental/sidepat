<?php

Route::namespace('Desempenio')->group(function() {
    // AUDITORIA DE DESEMPEÑO
    Route::post('/check-has-auditoria-desempenio', 'EnlacesController@checkHasAuditoria');
    Route::post('/desempenio/get-auditoria-by-funcionario-id', 'AuditoriasController@getAuditoriaByFuncionarioId');
    Route::post('/desempenio/get-general-data-cuestionario', 'AuditoriasController@getGeneralDataCuestionario');
    Route::post('/desempenio/get-preguntas', 'AuditoriasController@getPreguntas');
    Route::post('/desempenio/save-respuesta', 'AuditoriasController@saveRespuesta');
    Route::post('/desempenio/get-anexos', 'AuditoriasController@getAnexos');
    Route::post('/desempenio/delete-anexo', 'AuditoriasController@deleteAnexos');
    Route::post('/desempenio/enviar-cuestionario', 'AuditoriasController@enviarCuestionario');
    Route::post('/desempenio/descargar-respuestas', function() {
        $enlace = \DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = \DB::connection('sad')
            ->table('sad_eventos_bitacora')
            ->where('Descripcion', '=', 'Descarga de respuestas')
            ->first();
        \DB::connection('sad')->select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        $file= public_path(). "/desempenio/2020/{$auditoria}/acuses/respuestas_auditoria_desempenio.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_desempenio.pdf', $headers);
    });
    Route::post('/desempenio/descargar-acuses', function() {
        $enlace = \DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = \DB::connection('sad')
            ->table('sad_eventos_bitacora')
            ->where('Descripcion', '=', 'Descarga de acuse')
            ->first();
        \DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        $file= public_path()."/desempenio/2020/{$auditoria}/acuses/acuse_auditoria_desempenio.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_desempenio.pdf', $headers);
    });
});
