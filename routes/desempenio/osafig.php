<?php

//AUDITORIAS DESEMPEÑO
Route::namespace('Desempenio')->prefix('desempenio')->group(function() {
    Route::post('/get-auditorias', 'AuditoriasController@getAuditorias');
    Route::post('/create-auditorias', 'AuditoriasController@createAuditorias');
    Route::post('/get-auditoria-detalle', 'AuditoriasController@getAuditoriaDetalle');
    Route::post('/get-auditoria-fechas', 'AuditoriasController@getAuditoriaFechas');
    Route::post('/update-auditoria-fechas', 'AuditoriasController@updateAuditoriaFechas');
    Route::post('/get-enlace-asignado', 'AuditoriasController@getEnlaceAsignado');
    Route::post('/get-enlaces-ente', 'AuditoriasController@getEnlacesEnte');
    Route::post('/update-enlace-auditoria', 'AuditoriasController@updateEnlace');
    Route::get('/descargar-accesos', 'AuditoriasController@decargarAccesos');
    Route::get('/get-jefe-auditoria', 'AuditoriasController@getJefeAuditoria');
    Route::get('/get-auditores', 'AuditoriasController@getAuditores');
    Route::post('/update-auditor', 'AuditoriasController@updateAuditor');
    Route::post('/get-prorrogas', 'AuditoriasController@getProrrogas');
    Route::post('/get-etapa-auditoria', 'AuditoriasController@getEtapaAuditoria');
    Route::post('/add-prorroga', 'AuditoriasController@addProrrogas');
    Route::post('/edit-prorroga', 'AuditoriasController@updateProrrogas');
    Route::post('/delete-prorroga', 'AuditoriasController@deleteProrrogas');
    Route::post('/get-etapas-auditoria', 'AuditoriasController@getEtapasAuditoria');
    Route::post('/get-data-cuestionario', 'AuditoriasController@getDataCuestionario');
    Route::post('/update-cuestionario-auditoria', 'AuditoriasController@updateCuestionario');
    Route::post('/get-auditor-asignado', 'AuditoriasController@getAuditorAsignado');
    Route::post('/enviar-accesos', 'AuditoriasController@enviarAccesos');
    Route::post('/get-files-crp', 'AuditoriasController@getFilesCrp');
    Route::post('/descargar-respuestas-crp', 'AuditoriasController@descargarRespuestasCrp');
    Route::post('/descargar-acuse-crp', 'AuditoriasController@descargarAcuseCrp');
    Route::get('/get-data-auditorias-monitoreo', 'AuditoriasController@getDataAuditoriasMonitoreo');
    Route::post('/get-auditorias-monitoreo', 'AuditoriasController@getAuditoriasMonitoreo');
    Route::post('/get-respuestas-solventaciones', 'AuditoriasController@getRespuestasSolventaciones');
    Route::post('/get-respuestas-auditoria', 'AuditoriasController@getRespuestasInicales');
    Route::post('/validate-auditoria', 'AuditoriasController@validateAuditoria');
    Route::post('/get-general-data-auditoria', 'AuditoriasController@getGeneralDataAuditoria');
    Route::post('/save-valoracion', 'AuditoriasController@saveValoracion');
    Route::post('/enviar-revision', 'AuditoriasController@enviarRevision');
    Route::post('/generate-crp', 'AuditoriasController@generateCrp');
    Route::post('/descargar-crp', 'AuditoriasController@descargarCrp');
    Route::post('/mark-sent-crp', 'AuditoriasController@markSentCrp');
    // RUTAS PARA EL AUDITOR
    Route::prefix('/auditor')->group(function() {
        Route::post('/check-has-auditoria-desempenio', 'AuditoresController@checkHasAuditoriaDesempenio');
        Route::post('/get-respuestas-solventaciones', 'AuditoresController@getRespuestasSolventaciones');
        Route::post('/get-respuestas-auditoria', 'AuditoresController@getRespuestasInicales');
        Route::post('/validate-auditoria', 'AuditoresController@validateAuditoria');
        Route::post('/get-general-data-auditoria', 'AuditoresController@getGeneralDataAuditoria');
        Route::post('/get-preguntas', 'AuditoriasController@getPreguntas');
        Route::post('/get-anexos', 'AuditoriasController@getAnexos');
        Route::post('/save-valoracion', 'AuditoresController@saveValoracion');
        Route::post('/enviar-revision', 'AuditoresController@enviarRevision');
    });
});
