<?php

use Illuminate\Http\Request;

Route::post('/login', 'AuthController@login');

// --------------------------------------------------------------
//  RUTAS PARA USUARIOS DE OSAFIG
// --------------------------------------------------------------
Route::middleware(['auth:api'])->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/get-platforms', 'HomeController@getPlatforms');
    Route::post('/get-data-plataformas', 'HomeController@getDataPlataformas');
    Route::post('/get-user-platform-rol', 'HomeController@getUserPlatformRol');
    Route::post('/get-user-platform-rol-declaracion', 'HomeController@getUserPlatformRolDeclaracion');
    Route::get('/get-ejercicio-fiscal', 'EjerciciosController@getEjercicioFiscal');
    Route::get('/get-ejercicios-fiscales', 'EjerciciosController@getEjerciciosFiscales');
    Route::get('/get-entidades', 'EntidadesController@getEntidades');
    // RUTAS DE ADMINISTRACION TI
    require __DIR__ . '/administracion_ti/routes.php';
    // RUTAS DE  AUDITORIA DE CONTROL INTERNO
    require __DIR__ . '/control_interno/osafig.php';
    // RUTAS DE AUDITORIA DE DESEMPEÑO
    require __DIR__ . '/desempenio/osafig.php';
    //RUTAS DE DECLARACION PATRIMONIAL
    require __DIR__ . '/declaracion/routes.php';
});

// --------------------------------------------------------------
//  RUTAS PARA USUARIOS ENLACE
// --------------------------------------------------------------
Route::middleware(['auth_enlace'])->group(function () {
    // RUTAS DE AUDITORIA DE CONTORL INTERNO
    require __DIR__ . '/control_interno/enlaces.php';
    // RUTAS DE AUDITORIA DE DESEMPEÑO
    require __DIR__ . '/desempenio/enlaces.php';
});
