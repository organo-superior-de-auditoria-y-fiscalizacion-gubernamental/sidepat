<?php

// ----------------------------------------------------------------------
//    RUTAS DE CONTROL INTERNO PARA USUARIOS DE OSAFIG
// ----------------------------------------------------------------------
Route::namespace('ControlInterno')->group(function() {
    // RUTAS PARA EL RESPONSABLE DE AUDITORIA
    Route::post('/get-auditorias', 'AuditoriasController@getAuditorias');
    Route::post('/create-auditorias', 'AuditoriasController@createAuditorias');
    Route::post('/get-auditoria-detalle', 'AuditoriasController@getAuditoriaDetalle');
    Route::post('/get-auditoria-fechas', 'AuditoriasController@getAuditoriaFechas');
    Route::post('/update-auditoria-fechas', 'AuditoriasController@updateAuditoriaFechas');
    Route::get('/get-jefe-auditoria', 'AuditoriasController@getJefeAuditoria');
    Route::get('/get-auditores', 'AuditoriasController@getAuditores');
    Route::post('/update-auditor', 'AuditoriasController@updateAuditor');
    Route::post('/get-auditor-asignado', 'AuditoriasController@getAuditorAsignado');
    Route::post('/editar-generales', 'AuditoriasController@editarGenerales');
    Route::post('/get-enlace-asignado', 'AuditoriasController@getEnlaceAsignado');
    Route::post('/get-enlaces-ente', 'AuditoriasController@getEnlacesEnte');
    Route::post('/update-enlace-auditoria', 'AuditoriasController@updateEnlace');
    Route::get('/descargar-accesos', 'AuditoriasController@decargarAccesos');
    Route::post('/enviar-accesos', 'AuditoriasController@enviarAccesos');
    Route::post('/get-prorrogas', 'AuditoriasController@getProrrogas');
    Route::post('/get-etapa-auditoria', 'AuditoriasController@getEtapaAuditoria');
    Route::post('/get-etapas-auditoria', 'AuditoriasController@getEtapasAuditoria');
    Route::post('/add-prorroga', 'AuditoriasController@addProrrogas');
    Route::post('/edit-prorroga', 'AuditoriasController@updateProrrogas');
    Route::post('/delete-prorroga', 'AuditoriasController@deleteProrrogas');
    Route::post('/get-respuestas-solventaciones', 'AuditoriasController@getRespuestasSolventaciones');
    Route::post('/get-respuestas-auditoria', 'AuditoriasController@getRespuestasInicales');
    Route::post('/validate-auditoria', 'AuditoriasController@validateAuditoria');
    Route::post('/get-general-data-auditoria', 'AuditoriasController@getGeneralDataAuditoria');
    Route::post('/responsable/get-temas-cuestionario', 'AuditoriasController@getTemasCuestionario');
    Route::post('/responsable/get-preguntas-tema', 'AuditoriasController@getPreguntasTemaRevision');
    Route::post('/get-files-crp', 'AuditoriasController@getFilesCrp');
    Route::post('/descargar-respuestas-crp', 'AuditoriasController@descargarRespuestasCrp');
    Route::post('/descargar-acuse-crp', 'AuditoriasController@descargarAcuseCrp');
    Route::get('/get-data-auditorias-monitoreo', 'AuditoriasController@getDataAuditoriasMonitoreo');
    Route::post('/get-auditorias-monitoreo', 'AuditoriasController@getAuditoriasMonitoreo');
    Route::post('/generate-crp', 'AuditoriasController@generateCrp');
    Route::post('/descargar-crp', 'AuditoriasController@descargarCrp');
    Route::post('/mark-sent-crp', 'AuditoriasController@markSentCrp');
    //MODULO DE ENLACES
    Route::post('/get-enlaces', 'EnlacesController@getEnlaces');
    Route::post('/create-enlace', 'EnlacesController@createEnlace');
    Route::post('/get-enlace', 'EnlacesController@getEnlace');
    Route::post('/update-enlace', 'EnlacesController@updateEnlace');
    Route::post('/save-valoracion', 'AuditoriasController@saveValoracion');
    Route::post('/get-expedientes', 'AuditoriasController@getExpedientes');
    // RUTAS PARA EL AUDITOR
    Route::prefix('/auditor')->group(function() {
        Route::post('/check-has-auditoria-control-interno', 'AuditoresController@checkHasAuditoriaControlInterno');
        Route::post('/get-respuestas-solventaciones', 'AuditoresController@getRespuestasSolventaciones');
        Route::post('/get-respuestas-auditoria', 'AuditoresController@getRespuestasInicales');
        Route::post('/validate-auditoria', 'AuditoresController@validateAuditoria');
        Route::post('/get-general-data-auditoria', 'AuditoresController@getGeneralDataAuditoria');
        Route::post('/get-temas-cuestionario', 'AuditoriasController@getTemasCuestionario');
        Route::post('/get-preguntas-tema', 'AuditoresController@getPreguntasTemaRevision');
        Route::post('/get-anexos', 'AuditoriasController@getAnexos');
        Route::post('/get-opciones-pregunta', 'AuditoriasController@getOpcionesPregunta');
        Route::post('/save-valoracion', 'AuditoresController@saveValoracion');
        Route::post('/get-anexo', 'AuditoresController@getAnexo');
        Route::post('/descargar-anexo', function() {
            if(request()->has('tipo_auditoria')) {
                $anexo = \DB::connection('sad')->table('sad_anexo_det')->where('AnexoID', request()->anexo_id)->first();
            } else {
                $anexo = \DB::table('control_anexo_det')->where('AnexoID', request()->anexo_id)->first();
            }
            $file = public_path().'/'.$anexo->Ruta;
            return \Response::download($file, $anexo->NombreArchivo);
        });
        Route::post('/enviar-revision', 'AuditoresController@enviarRevision');
    });
});
