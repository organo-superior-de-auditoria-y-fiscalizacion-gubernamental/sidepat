<?php

Route::namespace('ControlInterno')->group(function () {
    Route::post('/enlace', 'EnlacesController@getDataEnlaceByToken');
    Route::post('/check-has-auditoria', 'EnlacesController@checkHasAuditoria');
    Route::post('/get-auditoria-by-funcionario-id', 'AuditoriasController@getAuditoriaByFuncionarioId');
    Route::post('/get-general-data-cuestionario', 'AuditoriasController@getGeneralDataCuestionario');
    Route::post('/get-temas-cuestionario', 'AuditoriasController@getTemasCuestionario');
    Route::post('/get-preguntas-tema', 'AuditoriasController@getPreguntasTema');
    Route::post('/get-opciones-pregunta', 'AuditoriasController@getOpcionesPregunta');
    Route::post('/save-respuesta', 'AuditoriasController@saveRespuesta');
    Route::post('/get-anexos', 'AuditoriasController@getAnexos');
    Route::post('/delete-anexo', 'AuditoriasController@deleteAnexos');
    Route::post('/enviar-cuestionario', 'AuditoriasController@enviarCuestionario');
    Route::post('/descargar-respuestas', function () {
        $enlace = DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = \DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de respuestas')
            ->first();
        DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        $file = public_path() . "/control_interno/2020/{$auditoria}/acuses/respuestas_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'respuestas_auditoria_control_interno.pdf', $headers);
    });
    Route::post('/descargar-acuses', function () {
        $enlace = \DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $evento_descarga = DB::table('control_eventos_bitacora_cat')
            ->where('Descripcion', '=', 'Descarga de acuse')
            ->first();
        \DB::select('call sp_entradaBitacora(?, ?, ?)', [
            $evento_descarga->EventoID,
            null,
            $enlace->FuncionarioID
        ]);
        $auditoria = request()->auditoria;
        $file = public_path() . "/control_interno/2020/{$auditoria}/acuses/acuse_auditoria_control_interno.pdf";
        $headers = ['Content-Type: application/pdf'];
        return \Response::download($file, 'acuse_auditoria_control_interno.pdf', $headers);
    });
});
