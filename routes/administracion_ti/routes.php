<?php

Route::namespace('AdministracionTi')->prefix('administracion-ti')->group(function() {
    Route::post('/get-users', 'UsersController@getUsers');
    Route::post('/provide-access-to-sdp', 'PrivilegiosController@provideAccessToSdp');
    Route::get('/get-user-roles', 'UsersController@getRoles');
    Route::post('/save-user-rol', 'UsersController@saveRoles');
    Route::post('/get-tipo-usuarios', 'UsersController@getTipoUsuarios');
    Route::post('/get-platform', 'UsersController@getPlatform');
    Route::post('/get-users-privilegios', 'UsersController@getUsersPrivilegios');
    Route::post('/add-user-privilegio', 'UsersController@addUserPrivilegio');
    Route::post('/remove-user-privilegio', 'UsersController@removeUserPrivilegio');
    Route::post('/update-password', 'UsersController@updatePassword');
});
