import './bootstrap';
import Vue from 'vue';

// Importar routes.js
import Routes from '@/js/routes.js';

// Importar componente App
import App from '@/js/views/App';

// Importar fuente de datos store
import {store} from '@/js/store/store';

const app = new Vue({
    el: '#app',
    router: Routes,
    store,
    render: h => h(App),
});

export default app;
