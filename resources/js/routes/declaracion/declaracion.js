// --------------------------------------------------------
//              SECCION DE USUARIOS DECLARACION
// --------------------------------------------------------
import IndexDeclaracion                             from "@/js/views/dashboard/declaracion/Declaracion";
import IndexDeclaracionAdministrador                from "@/js/views/dashboard/declaracion/DeclaracionAdministrador";
import IndexDeclaracionAdministradorPresentar       from "@/js/views/dashboard/declaracion/IndexDeclaracionAdministradorPresentar";
import IndexDeclaracionAdministradorPanel           from "@/js/views/dashboard/declaracion/IndexDeclaracionAdministradorPanel";
import IndexDeclaracionAdministradorPanelUsuarios   from "@/js/views/dashboard/declaracion/IndexDeclaracionAdministradorPanelUsuarios";
import DatosGenerales                               from "@/js/views/dashboard/declaracion/DatosGenerales";
import DomicilioDeclarante                          from "@/js/views/dashboard/declaracion/DomicilioDeclarante";
import DatosCurriculares                            from "@/js/views/dashboard/declaracion/DatosCurriculares";
import DatosCargo                                   from "@/js/views/dashboard/declaracion/DatosCargo";
import ExperienciaLaboral                           from "@/js/views/dashboard/declaracion/ExperienciaLaboral";
import DatosPareja                                  from "@/js/views/dashboard/declaracion/DatosPareja";
import DatosDependiente                             from "@/js/views/dashboard/declaracion/DatosDependiente";
import Ingresos                                      from "@/js/views/dashboard/declaracion/Ingresos";
import ServidorPublicoPasado                        from "@/js/views/dashboard/declaracion/ServidorPublicoPasado";
import BienesInmuebles                              from "@/js/views/dashboard/declaracion/BienesInmuebles";
import Vehiculos                                    from "@/js/views/dashboard/declaracion/Vehiculos";
import BienesMuebles                                from "@/js/views/dashboard/declaracion/BienesMuebles";
import Inversiones                                  from "@/js/views/dashboard/declaracion/Inversiones";
import Adeudos                                      from "@/js/views/dashboard/declaracion/Adeudos";
import PrestamosComodatos                           from "@/js/views/dashboard/declaracion/PrestamosComodatos";
import ParticipacionEmpresa                         from "@/js/views/dashboard/declaracion/ParticipacionEmpresa";
import ParticipacionInstituciones                   from "@/js/views/dashboard/declaracion/ParticipacionInstituciones";
import ApoyosBeneficios                             from "@/js/views/dashboard/declaracion/ApoyosBeneficios";
import Representacion                               from "@/js/views/dashboard/declaracion/Representacion";
import ClientesPrincipales                          from "@/js/views/dashboard/declaracion/ClientesPrincipales";
import BeneficiosPrivados                           from "@/js/views/dashboard/declaracion/BeneficiosPrivados";
import Fideicomisos                                 from "@/js/views/dashboard/declaracion/Fideicomisos";
import DeclaracionSat                               from "@/js/views/dashboard/declaracion/DeclaracionSat";

// Importamos middlewares
import authMiddleware        from '@/js/middleware/auth';

let middleware_rules = [
    authMiddleware,
];

const basePath = '/panel/sdp/presentar';

const routesDeclaracionUsuario = [
    {
        path: `${basePath}`,
        name: 'estatus-declaracion',
        component: IndexDeclaracion,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `/panel/sdp/administrador`,
        name: 'declaracion-administrador',
        component: IndexDeclaracionAdministrador,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `/panel/sdp/administrador/panel`,
        name: 'declaracion-administrador-panel',
        component: IndexDeclaracionAdministradorPanel,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `/panel/sdp/administrador/panel/usuarios`,
        name: 'declaracion-administrador-panel-usuarios',
        component: IndexDeclaracionAdministradorPanelUsuarios,
        meta: {
            middleware: middleware_rules
        },
    },
    // TODO: pendiente hacer el panel de administración del contralor
    //declaracion-administrador-panel-estadisticas
    {
        path: `/panel/sdp/administrador/presentar`,
        name: 'estatus-declaracion-administrador',
        component: IndexDeclaracionAdministradorPresentar,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/datos-generales`,
        name: 'datos-generales',
        component: DatosGenerales,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/domicilio-declarante`,
        name: 'domicilio-declarante',
        component: DomicilioDeclarante,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/datos-curriculares`,
        name: 'datos-curriculares',
        component: DatosCurriculares,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/datos-cargo`,
        name: 'datos-cargo',
        component: DatosCargo,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/experiencia-laboral`,
        name: 'experiencia-laboral',
        component: ExperienciaLaboral,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/datos-pareja`,
        name: 'datos-pareja',
        component: DatosPareja,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/datos-dependiente`,
        name: 'datos-dependiente',
        component: DatosDependiente,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/ingresos`,
        name: 'ingresos',
        component: Ingresos,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/servidor-publico-pasado`,
        name: 'servidor-publico-pasado',
        component: ServidorPublicoPasado,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/bienes-inmuebles`,
        name: 'bienes-inmuebles',
        component: BienesInmuebles,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/vehiculos`,
        name: 'vehiculos',
        component: Vehiculos,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/bienes-muebles`,
        name: 'bienes-muebles',
        component: BienesMuebles,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/inversiones`,
        name: 'inversiones',
        component: Inversiones,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/adeudos-pasivos`,
        name: 'adeudos-pasivos',
        component: Adeudos,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/prestamo-comodato`,
        name: 'prestamo-comodato',
        component: PrestamosComodatos,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/participacion-empresas`,
        name: 'participacion-empresas',
        component: ParticipacionEmpresa,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/participacion-instituciones`,
        name: 'participacion-instituciones',
        component: ParticipacionInstituciones,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/apoyos-beneficios`,
        name: 'apoyos-beneficios',
        component: ApoyosBeneficios,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/representacion`,
        name: 'representacion',
        component: Representacion,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/clientes-principales`,
        name: 'clientes-principales',
        component: ClientesPrincipales,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/beneficios-privados`,
        name: 'beneficios-privados',
        component: BeneficiosPrivados,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/fideicomisos`,
        name: 'fideicomisos',
        component: Fideicomisos,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/declaracion-sat`,
        name: 'subir-declaracion-sat',
        component: DeclaracionSat,
        meta: {
            middleware: middleware_rules
        },
    }
];

export { routesDeclaracionUsuario };
