import Index                 from '@/js/views/dashboard/administracion_ti/index';
import Privilegios           from '@/js/views/dashboard/administracion_ti/privilegios';
import PrivilegiosPlataforma from '@/js/views/dashboard/administracion_ti/privilegiosPlataforma';
import RolesUsuario          from '@/js/views/dashboard/administracion_ti/RolesUsuario';
import Usuarios              from '@/js/views/dashboard/administracion_ti/Usuarios';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

const basePath = '/panel/sati';

const routesAdministradorTi = [
    {
        path: `${basePath}`,
        name: 'plataforma-admon',
        component: Index,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/privilegios`,
        name: 'administracion-ti-privilegios',
        component: Privilegios,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/privilegios/plataforma/:platform`,
        name: 'administracion-ti-privilegios-plataforma',
        component: PrivilegiosPlataforma,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/privilegios/roles`,
        name: 'administracion-roles-usuario',
        component: RolesUsuario,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    },
    {
        path: `${basePath}/usuarios`,
        name: 'administracion-ti-usuarios',
        component: Usuarios,
        meta: {
            middleware: [
                authMiddleware,
            ]
        },
    }
]

export { routesAdministradorTi };
