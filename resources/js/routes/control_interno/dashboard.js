// Componentes de Dashboard
import ErrorDashboard from '@/js/views/404_Dashboard.vue';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

// Rutas Extendidas de Dashboard
import { routesControlInternoResponsable } from './responsable';
import { routesControlInternoAuditor } from './auditor';
import { routesControlInternoEnlace } from './enlace';

export default [
    ...routesControlInternoResponsable,
    ...routesControlInternoEnlace,
    ...routesControlInternoAuditor,
    {
        path: '*',
        name: '404-dashboard',
        component: ErrorDashboard,
        meta: {
            middleware: authMiddleware,
        }
    },
]
