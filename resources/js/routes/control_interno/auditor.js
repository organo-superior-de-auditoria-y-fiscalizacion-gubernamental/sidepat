// ----------------------------------------------
//              SECCION DE AUDITOR
// ----------------------------------------------
import IndexAuditor from '@/js/views/dashboard/indexAuditor';
import IndexControlInterno from "@/js/views/dashboard/auditoria_control_interno/Auditor/IndexControlInterno";
import ControlInternoPreviewAnexo from "@/js/views/dashboard/auditoria_control_interno/Auditor/ControlInternoPreviewAnexo";
import RespuestaInicialControlInterno from '@/js/views/dashboard/auditoria_control_interno/Auditor/RespuestaInicialControlInterno';
import RespuestaInicialControlInternoDetalleAuditoria from '@/js/views/dashboard/auditoria_control_interno/Auditor/RespuestaInicialControlInternoDetalleAuditoria';
import SolventacionRecomendacionesControlInterno from '@/js/views/dashboard/auditoria_control_interno/Auditor/SolventacionRecomendacionesControlInterno';

// Importamos middlewares
import authMiddleware        from '@/js/middleware/auth';
import authAuditorMiddleware from '@/js/middleware/authAuditorMiddleware';

let middleware_rules = [
    authMiddleware,
    authAuditorMiddleware
];

const basePath = '/panel/ci/auditor';

const routesControlInternoAuditor = [
    {
        path: `${basePath}`,
        name: 'dashboard_auditor_control_interno',
        component: IndexControlInterno,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/preview-anexo/:anexo_hash`,
        name: 'dashboard_auditor_control_interno_preview_anexo',
        component: ControlInternoPreviewAnexo,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path:  `${basePath}/respuesta-inicial`,
        name: 'dashboard_auditor_respuesta_inicial_control_interno',
        component: RespuestaInicialControlInterno,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path:  `${basePath}/respuesta-inicial/:auditoria`,
        name: 'dashboard_auditor_respuesta_inicial_control_interno_detalle',
        component: RespuestaInicialControlInternoDetalleAuditoria,
        meta: {
            middleware: middleware_rules
        },
    },
    {
        path: `${basePath}/solventacion-recomendaciones`,
        name: 'dashboard_auditor_modulo_solventacion_recomendaciones_control_interno',
        component: SolventacionRecomendacionesControlInterno,
        meta: {
            middleware: middleware_rules
        },
    },
];

export { routesControlInternoAuditor };
