// ------------------------------------------------------------------------------------------------------
//                      SECCION DE USUARIO RESPONSABLE DE AUDITORIA
// ------------------------------------------------------------------------------------------------------
import Index                            from '@/js/views/dashboard/auditoria_desempenio/Responsable/index.vue';
import ModuloAuditorias                 from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloAuditorias.vue';
import ModuloEnlaces                    from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloEnlaces.vue';
import ModuloEnlacesCrear               from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloEnlacesCrear.vue';
import ModuloEnlacesEditar              from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloEnlacesEditar.vue';
import ModuloAuditoriasDetalle          from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloAuditoriasDetalle.vue';
import ModuloRespuestasIniciales        from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloRespuestasIniciales.vue';
import ModuloRespuestasInicialesDetalle from "@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloRespuestasInicialesDetalle";
import DesempenioPreviewRespuestas      from '@/js/views/dashboard/auditoria_desempenio/Responsable/DesempenioPreviewRespuestas.vue';
import DesempenioPreviewAcuse           from '@/js/views/dashboard/auditoria_desempenio/Responsable/DesempenioPreviewAcuse.vue';
import DesempenioPreviewAnexo           from '@/js/views/dashboard/auditoria_desempenio/Responsable/DesempenioPreviewAnexo.vue';
import ModuloMonitoreo                  from '@/js/views/dashboard/auditoria_desempenio/Responsable/ModuloMonitoreo.vue';


// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';
import authResponsableMiddleware from '@/js/middleware/authResponsableMiddleware';

const basePath = '/panel/sad/responsable';
// Middleware
const middlewares = [
    authMiddleware,
    authResponsableMiddleware
];

const routesDesempenioResponsable = [
    {
        path: `${basePath}`,
        name: 'auditoria-desempenio',
        component: Index,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/auditorias`,
        name: 'modulo-auditorias-desempenio',
        component: ModuloAuditorias,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/enlaces`,
        name: 'modulo-enlaces-desempenio',
        component: ModuloEnlaces,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/enlaces/crear/:EnteID?`,
        name: 'modulo-enlaces-desempenio-crear',
        props: true,
        component: ModuloEnlacesCrear,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/enlaces/:enlace`,
        name: 'modulo-enlaces-desempenio-editar',
        component: ModuloEnlacesEditar,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/auditorias/:ejercicio/:auditoria`,
        name: 'modulo-auditorias-desempenio-detalle',
        component: ModuloAuditoriasDetalle,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/respuestas-iniciales`,
        name: 'modulo-respuesta-inicial-desempenio',
        component: ModuloRespuestasIniciales,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/respuestas-iniciales/:auditoria`,
        name: 'modulo-respuesta-inicial-desempenio-detalle',
        component: ModuloRespuestasInicialesDetalle,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/preview-respuestas/:ejercicio/:auditoria/:tipo_auditoria`,
        name: 'dashboard_desempenio_preview_respuestas',
        component: DesempenioPreviewRespuestas,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/preview-acuse/:ejercicio/:auditoria/:tipo_auditoria`,
        name: 'dashboard_desempenio_preview_acuse',
        component: DesempenioPreviewAcuse,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/monitoreo`,
        name: 'modulo-monitoreo-desempenio',
        component: ModuloMonitoreo,
        meta: {
            middleware: middlewares
        },
    },
    {
        path: `${basePath}/preview-anexo/:anexo_hash`,
        name: 'dashboard_desempenio_preview_anexo',
        component: DesempenioPreviewAnexo,
        meta: {
            middleware: middlewares
        },
    },
]

export { routesDesempenioResponsable };
