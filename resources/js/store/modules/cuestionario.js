export default  {
    state: {
        cuestionario: {
            noPreguntas: 0,
        },
        cuestionarioDesempenio: {
            preguntas: []
        },
        formTopic: {
            temaName: null,
            temaId: null,
            temaPunto: null,
            auditoriaId: null
        },
        pregunta: {},
        preguntaAnterior: {},
        preguntaSiguiente:{},
        anexos: [],
        anexosDesempenio: [],
        loading_anexos: false
    },
    mutations: {
        updateNoPreguntas(state, number) {
            state.cuestionario.noPreguntas = number;
        },
        resetNoPreguntas(state) {
            state.cuestionario.noPreguntas = 0;
        },
        updatePregunta(state, data) {
            state.pregunta = data;
        },
        updatePreguntaAnterior(state, data) {
            state.preguntaAnterior = data;
        },
        updatePreguntaSiguiente(state, data) {
            state.preguntaSiguiente = data;
        },
        resetPregunta(state) {
            state.pregunta = {};
        },
        updateFormTopic(state, data) {
            state.formTopic.temaName = data.tema.Tema;
            state.formTopic.temaId = data.tema.TemaID;
            state.formTopic.temaPunto = data.tema.Punto;
            state.formTopic.auditoriaId = data.auditoriaId;
        },
        resetFormTopic(state) {
            state.formTopic.temaName = null;
            state.formTopic.temaId = null;
            state.formTopic.temaPunto = null;
            state.formTopic.auditoriaId = null;
        },
        setAnexos(state, data) {
            state.anexos = data;
        },
        setAnexosDesempenio(state, data) {
            state.anexosDesempenio = data;
        },
        resetAnexos(state) {
            state.anexos = []
        },
        setLoadingAnexoTrue(state) {
            state.loading_anexos = true;
        },
        setLoadingAnexoFalse(state) {
            state.loading_anexos = false;
        },
        updatePreguntasDesempenio(state, preguntas) {
            state.cuestionarioDesempenio.preguntas = preguntas;
        }
    },
    actions: {
        getAnexos({ commit }, payload) {
            return new Promise((resolve) => {
                commit('setLoadingAnexoTrue');
                let data = {};
                if(localStorage.getItem('respuesta_id') !== null) {
                    data = {
                        api_token: localStorage.getItem('api_token'),
                        respuesta_id: localStorage.getItem('respuesta_id')
                    };
                } else {
                    data = {
                        api_token: localStorage.getItem('api_token'),
                        respuesta_id: payload.pregunta.RespuestaID
                    };
                }
                axios.post('/api/get-anexos', data).then(resp => {
                    localStorage.removeItem('respuesta_id');
                    commit('setAnexos', resp.data.anexos);
                    commit('setLoadingAnexoFalse');
                    resolve();
                }).catch((err) => {
                    console.log('getAnexos()');
                    console.log(err);
                });
            });
        },
        getAnexosAuditor({ commit }, payload) {
            return new Promise((resolve) => {
                commit('setLoadingAnexoTrue');
                let data = {};
                if(localStorage.getItem('respuesta_id') !== null) {
                    data = {
                        respuesta_id: localStorage.getItem('respuesta_id')
                    };
                } else {
                    data = {
                        respuesta_id: payload.pregunta.RespuestaID
                    };
                }
                axios.post('/api/auditor/get-anexos', data).then(resp => {
                    localStorage.removeItem('respuesta_id');
                    commit('setAnexos', resp.data.anexos);
                    commit('setLoadingAnexoFalse');
                    resolve();
                }).catch((err) => {
                    console.log('getAnexosAuditor()');
                    console.log(err);
                });
            });
        },
        getAnexosDesempenio({ commit }, payload) {
            return new Promise((resolve) => {
                commit('setLoadingAnexoTrue');
                let data = {};
                if(localStorage.getItem('respuesta_id') !== null) {
                    data = {
                        api_token: localStorage.getItem('api_token'),
                        respuesta_id: localStorage.getItem('respuesta_id')
                    };
                } else {
                    data = {
                        api_token: localStorage.getItem('api_token'),
                        respuesta_id: payload.pregunta.RespuestaID
                    };
                }
                axios.post('/api/desempenio/get-anexos', data).then(resp => {
                    localStorage.removeItem('respuesta_id');
                    commit('setAnexosDesempenio', resp.data.anexos);
                    commit('setLoadingAnexoFalse');
                    resolve();
                }).catch((err) => {
                    console.log('getAnexosDesempenio()');
                    console.log(err);
                });
            });
        },
        getAnexosDesempenioAuditor({ commit }, payload) {
            return new Promise((resolve) => {
                commit('setLoadingAnexoTrue');
                let data = {};
                if(localStorage.getItem('respuesta_id') !== null) {
                    data = {
                        respuesta_id: localStorage.getItem('respuesta_id')
                    };
                } else {
                    data = {
                        respuesta_id: payload.pregunta.RespuestaID
                    };
                }
                axios.post('/api/desempenio/auditor/get-anexos', data).then(resp => {
                    localStorage.removeItem('respuesta_id');
                    commit('setAnexosDesempenio', resp.data.anexos);
                    commit('setLoadingAnexoFalse');
                    resolve();
                }).catch((err) => {
                    console.log('getAnexosDesempenioAuditor()');
                    console.log(err);
                });
            });
        }
    },
    getters: {
        dataCuestionario(state) {
            return state.cuestionario;
        },
        dataFormTopic(state) {
            return state.formTopic;
        },
        dataPregunta(state) {
            return state.pregunta;
        },
        dataAnexosPregunta(state) {
            return state.anexos;
        },
        dataAnexosDesempenioPregunta(state) {
            return state.anexosDesempenio;
        },
        dataPreguntaAnterior(state) {
            return state.preguntaAnterior;
        },
        dataPreguntaSiguiente(state) {
            return state.preguntaSiguiente;
        },
        loadingAnexosPregunta(state) {
            return state.loading_anexos;
        },
        dataCuestionarioDesempenio(state) {
            return state.cuestionarioDesempenio.preguntas;
        }
    }
}
