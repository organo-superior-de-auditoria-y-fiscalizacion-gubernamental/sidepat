export default  {
    state: {
        //Expedientes
        hasExpedientesCrp: false,
        hasExpedientesSolventacion: false
    },
    mutations: {
        setHasExpedientesCrp(state, value) {
            state.hasExpedientesCrp = value;
        },
        setHasExpedientesSolventacion(value) {
            state.hasExpedientesSolventacion = value;
        }
    },
    actions: {
        getExpedientesAuditoria({ commit }, payload) {
            return new Promise((resolve) => {
                let data = payload;
                axios.post('/api/get-expedientes', data).then(resp => {
                    commit('setHasExpedientesCrp', resp.data.hasExpedienteCrp);
                    commit('setHasExpedientesSolventacion', resp.data.hasExpedientesSolventacion);
                    resolve();
                }).catch((err) => {
                    console.log(err);
                });
            });
        },
    },
    getters: {
        getHasExpedientesCrp(state) {
            return state.hasExpedientesCrp;
        },
        getHasExpedientesSolventacion(state) {
            return state.hasExpedientesSolventacion;
        }
    }
}
