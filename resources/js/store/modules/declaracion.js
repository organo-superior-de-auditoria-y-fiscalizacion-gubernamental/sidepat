export default  {
    state: {
        datos_declaracion: {},
        users: [],
        tipoDeclaracionConsultado: {}
    },
    mutations: {
        setUsersToDeclare(state, users) {
            state.users = users;
        },
        setTipoDeclaracionConsultado(state, tipo_consultado) {
            state.tipoDeclaracionConsultado = tipo_consultado;
        },
        setDatosDeclaracion(state, datos){
            state.datos_declaracion = datos;
        }
    },
    actions: {
        getUsersToDeclare({ commit }) {
            return new Promise((resolve) => {
                axios.get('/api/declaracion/admin/get-users-declaracion').then(resp => {
                    commit('setUsersToDeclare', resp.data.users);
                    resolve();
                }).catch(e => {
                    console.log(e.response.data.message);
                });
            });
        },
        getDatosDeclaracion({commit}){
            return new Promise((resolve) => {
                axios.post('/api/declaracion/get-data-declaracion').then(resp => {
                    commit('setDatosDeclaracion', resp.data.declaracion);
                    resolve();
                }).catch(e => {
                    console.log(e);
                })
            })
        }
    },
    getters: {
        getUsersToDeclare(state) {
            return state.users;
        },
        getTipoDeclaracionConsultado(state)
        {
            return state.tipoDeclaracionConsultado;
        },
        getDatosDeclaracion(state)
        {
            return state.datos_declaracion;
        }
    }
}
