// Middleware encargado de validar que solo el responsable acceda a sus rutas
export default function auth({next, router}) {
    let rol = localStorage.getItem('user_rol');
    if(rol === null) {
        return router.push({name: 'dashboard_enlace'});
    } else if(rol === 'auditor') {
        return router.push({name: 'dashboard_auditor'});
    } else {
        return next();
    }
}
