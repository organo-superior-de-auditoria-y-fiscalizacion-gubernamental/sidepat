// Middleware encargado de validar que solo el auditor acceda a sus rutas
export default function auth({next, router}) {
    let rol = localStorage.getItem('user_rol');
    if(rol === 'auditor') {
        return next();
    } else if(rol === 'responsable') {
        return router.push({name: 'dashboard'});
    } else {
        return router.push({name: 'dashboard_enlace'});
    }
}
