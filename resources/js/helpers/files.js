function makeFormData(data) {
    let fd = new FormData();
    for (const prop in data) {
        if(prop === 'files') {
            for (let i = 0; i < data[prop].length; i++) {
                fd.append('fileToUpload[]', data[prop][i]);
            }
        } else if(prop === 'pregunta') {
            fd.append(prop, JSON.stringify(data[prop]));
        } else {
            fd.append(prop, data[prop]);
        }
    }
    return fd;
}

const fileHelper = { makeFormData };
export default fileHelper;
