<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acuse de recibo - Sistema de Auditoría de Desempeño y Control Interno</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        #anexos {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #anexos td, #anexos th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        #anexos tr:nth-child(even){background-color: #f2f2f2;}

        #anexos tr:hover {background-color: #ddd;}

        #anexos th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #bdbdbd;
            color: white;
        }
    </style>
</head>
<body>
@php
    $GLOBALS['nombre_enlace'] = $enlace_auditoria->NombreCompleto;

    $date  = explode(' ', date('Y-m-d H:i:s'));
    $date2 = explode('-', $date[0]);
    $final_date = $date2[2].'-'.$date2[1].'-'.$date2[0].' '.$date[1];
    $GLOBALS['final_date'] = $final_date;
@endphp
<script type="text/php">
    if (isset($pdf)) {
        $pdf->page_script('
            if ($PAGE_NUM > 0) {
                $font = null;
                $size = 7;
                $pageText = "{$PAGE_NUM} de {$PAGE_COUNT}";
                $y = $pdf->get_height()-35;
                $x = 555;
                $pdf->text($x, $y, $pageText, $font, $size);
            }
            if ($PAGE_NUM == $PAGE_COUNT) {
                $font = null;
                $size = 7;
                $pageText = "Enlace: {$GLOBALS["nombre_enlace"]}. Fecha de finalización: {$GLOBALS["final_date"]}";
                $y = $pdf->get_height()-35;
                $x = 20;
                $pdf->text($x, $y, $pageText, $font, $size);
            }
        ');
    }
</script>
<div style="text-align: center;">
    <img src="img/LogoOsafig.PNG" width="300" height="120">
</div>
<div style="text-align: center; margin-top: 20px; margin-bottom: 20px;">
    <table style="margin-top: 10px; border-collapse: collapse;">
        <tr style="text-align: center; background-color: #7E042E !important; color: #FFFFFF;">
            <td style="font-size: 18px !important; font-weight: bold; text-align: center;">
                AUDITORÍA DE DESEMPEÑO {{$auditoria->Ejercicio}}
            </td>
        </tr>
        <tr style="text-align: center; background-color: #757575 !important; color: #FFFFFF;">
            <td style="font-size: 18px !important; font-weight: bold; text-align: center;">
                {{$auditoria->Entidad}} <br>
                <!--Acuse de recibo-->
            </td>
        </tr>
    </table>
</div>
<div style="width: 100%; margin-top: 30px;">
    @foreach($data as $pregunta)
        <div style="font-size: 13px; text-align: justify; font-weight: bold; margin-bottom: 5px;">
            {{$pregunta['pregunta']->NoPregunta.". ".$pregunta['pregunta']->Pregunta }}
        </div>
        <div style="font-size: 12px; text-align: justify; margin-left: 20px; margin-bottom: 10px;">
            {!! $pregunta['pregunta']->Respuesta !!}
        </div>
        <div style="font-size: 12px; text-align: justify; margin-left: 17px; margin-bottom: 20px;">
            @if($pregunta['pregunta']->No_Anexos == 0)
                <p style="margin-left: 3px; margin-top: 0px; font-weight: bold;">
                    Sin documentación anexa.
                </p>
            @else
                <div style="width: 99% !important; margin: 0 auto !important;">
                    <strong style="font-size: 13px;">Relación de archivos adjuntos</strong>
                    <table id="anexos">
                        <tr>
                            <th style="width: 30% !important;">Nombre del archivo</th>
                            <th style="width: 60% !important;">Huella SHA-2 (256)</th>
                        </tr>
                        @foreach($pregunta['anexos'] as $file)
                            <tr>
                                <td style="font-size: 12px !important; word-wrap: break-word !important; overflow-wrap: break-word !important;">{{$file->NombreArchivo}}</td>
                                <td style="font-size: 11px !important; word-wrap: break-word !important; overflow-wrap: break-word !important;">{{$file->Hash}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @endif
        </div>
    @endforeach
</div>
</body>
</html>
