<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acuse de recibo - Sistema de auditoría de desempeño y control interno</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        #anexos {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #anexos td, #anexos th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        #anexos tr:nth-child(even){background-color: #f2f2f2;}

        #anexos tr:hover {background-color: #ddd;}

        #anexos th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #bdbdbd;
            color: white;
        }
    </style>
</head>
<body>
<div style="text-align: center;">
    <img src="img/LogoOsafig.PNG" width="300" height="120">
</div>
<div style="text-align: center; margin-top: 90px; margin-bottom: 2px; font-size: 25px; border: 1px solid; font-weight:bold">
    ACUSE DE RECIBIDO
</div>
<p style="text-align: justify;">
    Se ha recibido el cuestionario con la información y documentación anexa, a través de la Plataforma de Evaluación al Desempeño y Control Interno, sin realizar juicios de tipo cualitativo o de fondo, para ser analizada y posteriormente valorada; por lo que este Órgano Fiscalizador se reserva el derecho de solicitar la información y documentación adicional, por este medio.
    Haciendo constar que el Enlace <strong>{{ $enlace_auditoria->NombreCompleto }}</strong> mediante la huella HASH que como anexos al cuestionario el Ente Fiscalizado presenta EN FORMATO ELECTRONICO, un total de {{ $noAnexosTotal }} archivos, los cuales se verificaron cuantitativamente.
</p>
<div style="width: 100%; margin-top: 180px;">
    <table class="row-table" style="margin-bottom: 40px;">
        <tr>
            <td style="width: 50%; text-align: center;">
                <div class="text-table" style="width: 100%; border-bottom: .1px solid;">
                    Colima, México. {{ date('d-m-Y H:i:s') }}
                </div>
            </td>
        </tr>
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 50%; text-align: center;">LUGAR Y FECHA</td>
        </tr>
    </table>
    <table class="row-table">
        <tr style="font-size: 10px; font-style: italic;">
            <td style="width: 100%; text-align: center;">
                <div style="background-color: #f5f5f5; width: 97%; height: 20px; padding-top: 9px; padding-bottom: 5px;">
                    SELLO DIGITAL
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: center;">
                <div class="text-table" style="width: 100%; border-bottom: .1px solid #fff; text-transform: uppercase;">
                    {{ $hash }}
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
