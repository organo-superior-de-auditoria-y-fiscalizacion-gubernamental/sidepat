<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accesos de enlace</title>
    <style>
        html, body {
            font-family: Tahoma, Geneva, sans-serif;
        }
        h1{
            font-size: 18px !important;
        }
        h2{
            font-size: 16px !important;
        }
        table{
            width: 100% !important;
        }
        tr{
            height: 30px !important;
        }
        td{
            font-size: 15px !important;
        }
        #watermark {
            position: fixed;
            bottom: -350px;
            right: -350px;
            width: 700px;
            height: 700px;
            opacity: .6;
        }
    </style>
</head>
<body>
<div id="watermark">
    <img src="img/greca_01.png">
</div>
<div style="text-align: center;">
    <img src="img/LogoOsafig.PNG" width="300" height="120">
</div>
<div style="text-align: center; margin-top: 10px; margin-bottom: 20px;">
    <table style="margin-top: 50px">
        <tr style="text-align: center">
            <td style="font-size: 18px !important; font-weight: bold">
                <center>AUDITORÍA DE DESEMPEÑO Y CONTROL INTERNO</center>
            </td>
        </tr>
        <tr style="text-align: center">
            <td style="font-size: 16px !important; font-weight: bold">
                <center>DATOS DE ACCESO AL SISTEMA</center>
            </td>
        </tr>
    </table>
</div>
<hr>
<div style="width: 100%; margin-top: 20px;">
    <p style="text-align: center">
        CONSTANCIA DE ENTREGA DE DATOS DE ACCESO
    </p>
    <p style="text-align: justify;">
        Este documento hace constar la entrega del usuario y contraseña al Enlace del Ente Público
        designado para el ingreso y administración de información de la Plataforma de Evaluación de
        Desempeño y Control Interno del Órgano Superior de Auditoría y Fiscalización Gubernamental,
        advirtiendo que es su completa responsabilidad el uso y resguardo de la misma, por lo que se
        recomienda no compartirla.
    </p>
    <table style="border-collapse: collapse; margin-top: 20px;">
        <tr>
            <td style="width: 50% !important; padding: 10px !important;">
                <strong>Ente Fiscalizable: </strong>
            </td>
            <td style="width: 50% !important; padding: 10px !important;">
                <i>{{ $data->Entidad }}</i>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px !important;">
                <strong>Enlace: </strong>
            </td>
            <td style="padding: 10px !important;">
                <i>{{ $data->NombreCompleto }}</i>
            </td>
        </tr>
        <tr>
            <td style="padding: 10px !important;">
                <strong>Cargo del Enlace: </strong>
            </td>
            <td style="padding: 10px !important;">
                <i>{{ $data->Puesto }}</i>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <div style="margin-top: 50px !important;">
                    <strong>https://auditoriaenlinea.osaf.gob.mx</strong> <br>
                    Correo electrónico: <strong>{{ $data->EmailInstitucional }}</strong> <br>
                    Contraseña: <strong>{{ Illuminate\Support\Facades\Crypt::decryptString($data->Password) }} </strong>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
