@component('mail::message')
<center>
    <table style="color: #000000 !important;">
        <tr>
            <td style="text-align: center">
                <img src="{{asset('img/osafig2018.png')}}" width="200">
            </td>
        </tr>
        <tr>
            <td style="text-align: center;"><strong>SISTEMA DE AUDITORÍA DE DESEMPEÑO Y CONTROL INTERNO</strong></td>
        </tr>
        <tr>
            <td style="text-align: center;"><strong>DATOS DE ACCESO AL SISTEMA</strong></td>
        </tr>
    </table>
    <br>
    <table style="color: #000000 !important;">
        <tr>
            <p style="text-align: center">
                CONSTANCIA DE ENTREGA DE DATOS DE ACCESO
            </p>
            <p style="text-align: justify">
                Este correo hace constar la entrega del usuario y contraseña al Enlace del Ente Público
                designado para el ingreso y administración de información de la Plataforma de Evaluación de
                Desempeño y Control Interno del Órgano Superior de Auditoría y Fiscalización Gubernamental,
                advirtiendo que es su completa responsabilidad el uso y resguardo de la misma, por lo que se
                recomienda no compartirla.
            </p>
        </tr>
        <tr>
            <td style="width: 50% !important;"><strong>Ente Fiscalizable:</strong></td>
            <td style="width: 50% !important; text-align: right;"><i>{{$data->Entidad}}</i></td>
        </tr>
        <tr>
            <td style="width: 50% !important;"><strong>Enlace:</strong></td>
            <td style="width: 50% !important; text-align: right;"><i>{{$data->NombreCompleto}}</i></td>
        </tr>
        <tr>
            <td style="width: 50% !important;"><strong>Cargo del Enlace: </strong></td>
            <td style="width: 50% !important; text-align: right;"><i>{{ $data->Puesto }}</i></td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <div style="margin-top: 20px !important;">
                    <strong>https://auditoriaenlinea.osaf.gob.mx</strong> <br>
                    Correo electrónico: <strong>{{ $data->EmailInstitucional }}</strong> <br>
                    Contraseña: <strong>{{ Illuminate\Support\Facades\Crypt::decryptString(($data->Password)) }} </strong>
                </div>
            </td>
        </tr>
    </table>
</center>

{{ config('app.name') }}
@endcomponent
