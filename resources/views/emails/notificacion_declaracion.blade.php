@component('mail::message')
    <center>
        <table style="color: #000000 !important;">
            <tr>
                <td style="text-align: center">
                    <img src="{{asset('img/osafig2018.png')}}" width="200">
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"><strong>AUDITORÍA EN LÍNEA</strong></td>
            </tr>
            <tr>
                <td style="text-align: center;"><strong></strong></td>
            </tr>
        </table>
        <br>
        <table style="color: #000000 !important;">
            <tr>
                <P style="text-align: right; font-weight: bold;">
                    CIRCULAR/OIC/01/2021
                </P>
                <p style="text-align: left; font-weight: bold;">
                    TRABAJADORES, SERVIDORES PÚBLICOS, DEL ÓRGANO SUPERIOR DE AUDITORÍA Y FISCALIZACIÓN GUBERNAMENTAL (OSAFIG)
                </p>
                <p style="text-align: left; font-weight: bold;">
                    P R E S E N T E S.-
                </p>
                <p style="text-align: justify">
                    Por medio del presente y en ejercicio de las facultades que me confiere el artículo 12, fracción XXV, del Reglamento Interior del Órgano Superior de Auditoría y Fiscalización Gubernamental, y con fundamento en lo dispuesto por los artículos 2 fracción X, 49 fracción I y 51 de la Ley General del Sistema Nacional Anticorrupción; 9 fracción II, 10 fracción I, 15, 30, 31, 32, 33, 34, 46 y 48 de la Ley General de Responsabilidades Administrativas; Normas Quinta y Vigésimo primera del Anexo Segundo "ACUERDO por el que se modifican los Anexos Primero y Segundo del Acuerdo por el que el Comité Coordinador del Sistema Nacional Anticorrupción emite el formato de declaraciones: de situación patrimonial y de intereses; y expide las normas e instructivo para su llenado y presentación".
                    Se les recuerda la obligación de presentar ante esta Contraloría Interna la Declaración de Situación Patrimonial y de Intereses, de modificación; toda vez que de no hacerlo se harán acreedores a las sanciones señaladas en el artículo 75 fracciones I, II, III, y IV, y articulo 78 fracciones I, II, III, y IV de la Ley General de Responsabilidades Administrativas.
                </p>
                <p style="text-align: justify">
                    El formato digital para la presentación de la declaración de patrimonial y de interés, se encuentra la plataforma digital SiDePat ubicada en el portal oficial del OSAFIG,
                    en el siguiente link: <a href="https://auditoriaenlinea.osaf.gob.mx">https://auditoriaenlinea.osaf.gob.mx</a>, al cual podrán ingresar con su <strong>correo oficial como usuario y contraseña.</strong>
                </p>
                <!-- TODO: Agregar el usuario y la contraseña -->
                <p style="text-align: center; margin-top: 10px; margin-bottom: 10px;">
                    Correo: {{ $user['Email'] }} <br>
                    Contraseña: {{ Illuminate\Support\Facades\Crypt::decryptString(($user['PasswordEncrypt'])) }}
                </p>
                <p style="text-align: justify">
                    Así mismo, se solicita a quienes estén obligados a presentar la de clarión de anual de sueldos y salarios presenten constancia del acuse de la presentación de la declaración del Impuesto Sobre la Renta, correspondiente al ejercicio fiscal de 2020, en caso de encontrarse en los supuestos del artículo 150 de la Ley del Impuesto Sobre la Renta y artículos 31 y 31 Código Fiscal de la Federación, y tener la obligación de presentarla ante el SAT.
                </p>
                <p style="text-align: justify">
                    Sin otro particular, reciban un cordial saludo.
                </p>
            </tr>
            <tr>
                <p style="text-align: center">
                    Atentamente. <br>
                    Colima, Col. a 17 de mayo de 2021.
                </p>
            </tr>
            <tr>
                <p style="text-align: center">
                    C.P. Miguel Ángel Preciado Cortes. <br>
                    Contralor Interno
                </p>
            </tr>
        </table>
    </center>

    Gracias.
    {{ config('app.name') }}
@endcomponent
