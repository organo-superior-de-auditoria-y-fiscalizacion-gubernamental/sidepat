<?php

namespace App\Http\Controllers\Api\Desempenio;

use App\Http\Controllers\Controller;
use App\Http\Repositories\{EjerciciosRepository};
use App\Http\Repositories\Desempenio\{AuditoriasRepository, EtapasRepository};
use Illuminate\Support\Facades\{DB, Log};

class AuditoresController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios
    protected $ar;  //Repositorio de datos de Auditorias
    protected $etr; //Repositorio de etapas de Auditorias

    public function __construct(EjerciciosRepository $er, AuditoriasRepository $ar, EtapasRepository $etr)
    {
        $this->er = $er;
        $this->ar = $ar;
        $this->etr = $etr;
    }

    /**
     * Revisa si el auditor tiene activa la auditoria de desempeño
     */
    public function checkHasAuditoriaDesempenio()
    {
        $user = request()->user();
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        //Validar si el usuario auditor tiene al menos una auditoria asignada
        $has_auditoria = DB::connection('sad')
            ->select('call sp_hasAuditoria(?,?,?,?)', [0, 0, $ejercicio_auditado->EjercicioID, $user->UsuarioID]);
        $asignado = false;
        if(count($has_auditoria) > 0) {
            $asignado = true;
        }
        return response()->json([
            'status' => 'Ok',
            'hasAudDesempenio' => $asignado
        ], 200);
    }

    /**
     * Obtener las respuestas iniciales y solventacion de recomendaciones
     */
    public function getRespuestasSolventaciones()
    {
        $user = request()->user();
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        //Respuestas iniciales
        $no_respuestas = DB::connection('sad')
            ->table('v_estatus_auditoria')
            ->where('EtapaActual', '=', 2)
            ->where('UsuarioActual', '=', $user->UsuarioID)
            ->where('EjercicioID', '=', $ejercicio_auditado->EjercicioID)
            ->count();
        // Solventaciones
        $no_solventaciones = DB::connection('sad')
            ->table('v_estatus_auditoria')
            ->where('EtapaActual', '=', 7)
            ->where('UsuarioActual', '=', $user->UsuarioID)
            ->where('EjercicioID', '=', $ejercicio_auditado->EjercicioID)
            ->count();

        return response()->json([
            'status' => 'Ok',
            'noRespuestas' => $no_respuestas,
            'noSolventaciones' => $no_solventaciones
        ], 200);
    }

    /**
     * Obtener respuestas iniciales de auditoria
     */
    public function getRespuestasInicales()
    {
        $user = request()->user();
        $ejercicio_auditado = $this->er->getEjercicioAuditado();
        $respuestas = DB::connection('sad')->select('call sp_obtenerAuditoriasPorEtapaActual(?,?)', [2, $ejercicio_auditado->EjercicioID]);
        $respuestas_auditor = [];
        for($i = 0; $i < count($respuestas); $i++) {
            if($respuestas[$i]->UsuarioActual == $user->UsuarioID) {
                array_push($respuestas_auditor, $respuestas[$i]);
            }
        }
        foreach ($respuestas_auditor as $respuesta) {
            $respuesta->preguntas = DB::connection('sad')->select('call sp_obtener_preguntas_auditoria(?)', [$respuesta->AuditoriaID]);
        }
        //Respuestas iniciales
        return response()->json([
            'status' => 'Ok',
            'respuestas' => $respuestas_auditor
        ], 200);
    }

    /**
     * Validar que la auditoria exista y este asignada al auditor
     */
    public function validateAuditoria()
    {
        $validate = false;
        $user = request()->user();
        $auditoria = DB::connection('sad')
            ->table('sad_asignacion_personal_auditoria')
            ->where('AuditoriaID', '=', request()->auditoria)
            ->where('Enlace', '=', 0)
            ->where('ResponsableAuditoria', '=', 0)
            ->where('UsuarioID', '=', $user->UsuarioID)
            ->whereNull('deleted_at')
            ->first();
        if(is_object($auditoria)) {
            $validate = true;
        }
        return response()->json([
            'status' => 'Ok',
            'validate' => $validate
        ], 200);
    }

    /**
     * Obtener el detalle de la auditoria
     */
    public function getGeneralDataAuditoria()
    {
        $auditoria = $this->ar->getAuditoriaDetalle(request()->auditoria);
        $enlace = DB::connection('main')
            ->table('osaf_v_enlaces')
            ->where('FuncionarioID', '=', $auditoria->Enlace)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'auditoria' => $auditoria,
            'enlace' => $enlace
        ]);
    }

    /**
     * Guardar valoración del auditor
     */
    public function saveValoracion()
    {
        try {
            $user = request()->user();
            //DATOS PREGUNTA
            $respuesta_id = request()->pregunta['RespuestaID'];
            $usuario_id = $user->UsuarioID;
            $recomendacion = request()->recomendacion ?? '';
            $valoracion = request()->valoracion;
            $puntuacion = (float) request()->puntuacion;
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoriaId);
            $auditoria_etapa_id = $etapa_actual->AuditoriaEtapaID;
            $recomendacion_id = request()->pregunta['RecomendacionIDAuditor'];
            // REGISTRAR LA RESPUESTA
            $resp = DB::connection('sad')->select('call sp_registrarRecomendacion(?,?,?,?,?,?,?,?)',[
                $respuesta_id,
                $usuario_id,
                'Auditor',
                $recomendacion,
                $valoracion,
                $puntuacion,
                $auditoria_etapa_id,
                $recomendacion_id
            ]);
            if($recomendacion_id !== null) {
                $recomendacion_id = $recomendacion_id;
            } else {
                $recomendacion_id = $resp[0]->recomendacion_id;
            }
            return response()->json([
                'status' => 'Ok',
                'recomendacion_id' => $recomendacion_id
            ], 200);
        }catch(\Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Envíar revisión al responsable de auditoria
     */
    public function enviarRevision()
    {
        try{
            DB::connection('sad')->beginTransaction();
            //Finalizar cuiestionario, cambiando la etapa
            $etapa_actual = $this->etr->getEtapaActualAuditoria(request()->auditoria);
            $etapa_id = $etapa_actual->EtapaID;
            DB::connection('sad')
                ->table('sad_auditoria_etapa_det')
                ->where('EtapaID', '=', $etapa_id)
                ->where('AuditoriaID', '=', request()->auditoria)
                ->update([
                    'Finalizado' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            DB::connection('sad')->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            Log::info("ERROR | {$e->getMessage()} | {$e->getLine()} | {$e->getFile()}");
            DB::connection('sad')->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
}
