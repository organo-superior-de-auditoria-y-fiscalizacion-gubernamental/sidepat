<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected $accesos_db;

    public function __construct()
    {
        $this->accesos_db = DB::connection('accesos');
    }

    /**
     * Obtener plataformas
     */
    public function getPlatforms()
    {
        $plataforms = $this->accesos_db
            ->table('osaf_plataforma_cat')
            ->orderBy('Descripcion', 'asc')
            ->get();
        return response()->json([
            'status'    => 'Ok',
            'platforms' => $plataforms
        ], 200);
    }

    /**
     * Obtener las plataformas a las que tiene acceso el usuario
     */
    public function getDataPlataformas()
    {
        $user = request()->user();
        $privilegios = $this->accesos_db->select('call sp_obtenerPrivilegiosUsuario(?)', [$user->UsuarioID]);
        $plataformasName = [];
        for($i = 0; $i < count($privilegios); $i++) {
            $plataforma = $privilegios[$i]->Plataforma;
            if (in_array($plataforma, $plataformasName) == false) {
                array_push($plataformasName, $plataforma);
            }
        }
        $plataformas = $this->accesos_db
            ->table('osaf_plataforma_cat')
            ->whereIn('NombrePlataforma', $plataformasName)
            ->orderBy('Descripcion', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'plataformas' => $plataformas
        ], 200);
    }

    /**
     * Obtener el rol del usuario en una plataforma dada
     */
    public function getUserPlatformRol()
    {
        $user = request()->user();
        $platform = $this->accesos_db
            ->table('osaf_plataforma_cat')
            ->where('NombrePlataforma', '=', request('plataforma'))
            ->first();
        $userType = $this->accesos_db->select('call sp_obtenerTipoUsuarioPlataforma(?, ?)', [$user->UsuarioID, $platform->PlataformaID]);
        return response()->json([
            'status' => 'Ok',
            'userType' => $userType[0]
        ], 200);
    }

    /**
     * Obtener los roles del usuario en una plataforma dada
     */
    public function getUserPlatformRolDeclaracion()
    {
        $user = request()->user();
        $userTypes = [];
        $platform = $this->accesos_db
            ->table('osaf_plataforma_cat')
            ->where('NombrePlataforma', '=', request('plataforma'))
            ->first();
        $userType = $this->accesos_db->select('call sp_obtenerTipoUsuarioPlataforma(?, ?)', [$user->UsuarioID, $platform->PlataformaID]);
        for($i = 0; $i < count($userType); $i++) {
            array_push($userTypes, $userType[$i]);
        }
        return response()->json([
            'status' => 'Ok',
            'userTypes' => $userTypes
        ], 200);
    }
}
