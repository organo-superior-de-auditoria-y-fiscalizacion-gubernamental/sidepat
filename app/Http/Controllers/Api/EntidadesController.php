<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class EntidadesController extends Controller
{
    /**
     * Obtener las entidades disponibles, tomadas como referencia de la auditoría de desempeño
     */
    public function getEntidades()
    {
        $entidades = DB::connection('main')
            ->table('osaf_entidades_fiscalizables')
            ->whereIn('EntidadID', [
                257, // CIAPACOV
                260, // IPECOL
                281, // DIF ESTATAL
                287, // PODER LEGISLATIVO
                288, // PODER JUDICIAL
                289, // COMISIÓN DE DERECHOS HUMANOS DEL ESTADO
                290, // INFOCOL
                291, // IEE
                294, // TAE
                295, // TJAE
                296, // TEE
                299, // MPIO ARMERIA
                300, // MPIO COLIMA
                301, // MPIO COIMALA
                302, // MPIO COQUIMATLAN
                303, // MPIO CUAUHTEMOC
                304, // MPIO IXTLAHUACAN
                305, // MPIO MANZANILLO
                306, // MPIO MINATITLAN
                307, // MPIO TECOMAN
                308, // MPIO VILLA DE ALVAREZ
                311, // CAPACO
                312, // COMAPAL
                313, // CAPAC
                314, // COMAPAC
                315, // CAPAI
                316, // CAPAMI
                317, // CAPDAM
                318, // COMAPAT
                353, // FGE
                356, // PODER EJECIUTIVO
            ])
            ->select('EntidadID', 'Nombre')
            ->orderBy('Nombre', 'asc')
            ->get();
        return response()->json([
            'status' => 'Ok',
            'entidades' => $entidades
        ], 200);
    }
}
