<?php

namespace App\Http\Controllers\Api\AdministracionTi;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;

class PrivilegiosController extends Controller
{
    protected $main_db;
    protected $accesos_db;

    public function __construct()
    {
        $this->main_db = DB::connection('main');
        $this->accesos_db = DB::connection('accesos');
    }

    /**
     * Asinar privilegio de acceso a declaración patrimonial
     */
    public function provideAccessToSdp()
    {
        try {
            $users = request()->users;
            for($i = 0; $i < count($users); $i++) {
                //Log::info("UserId: {$i}");
            }
            // call sp_asignarPrivilegiosUsuario(60, 4, 8);
        } catch(Exception $e) {
            Log::error("ERROR | {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
}
