<?php

namespace App\Http\Controllers\Api\AdministracionTi;

use App\Http\Repositories\AdministracionTi\UserRepository;
use Illuminate\Support\Facades\{Crypt, DB, Log, Hash};
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Exception;

class UsersController extends Controller
{
    protected $main_db;
    protected $accesos_db;
    protected $user_repository;

    public function __construct(UserRepository $user_repository)
    {
        $this->main_db         = DB::connection('main');
        $this->accesos_db      = DB::connection('accesos');
        $this->user_repository = $user_repository;
    }

    /**
     * Obtener a todos los usuarios de osafig
     */
    public function getUsers()
    {
        $users = $this->main_db->table('osaf_usuarios')
            ->where(function($query) {
                if( is_null(request('search')) == false) {
                    $query->where('Nombres', 'like', '%'. request('search') . '%')
                          ->orWhere('PrimerApellido', 'like', '%'. request('search') . '%')
                          ->orWhere('SegundoApellido', 'like', '%'. request('search') . '%');
                }
            })
            ->where(function($query) {
                if(request()->has('estado_filter')) {
                    if( count(request('estado_filter')) > 0) {
                        $query->whereIn('Activo', request('estado_filter'));
                    }
                }
            })
            ->orderBy('Nombres', 'asc')
            ->get([
                'UsuarioID',
                'Nombres',
                'PrimerApellido',
                'SegundoApellido',
                'Email',
                'Activo',
                'PasswordEncrypt'
            ]);
        for($i = 0; $i < count($users); $i++) {
            $user = $users[$i];
            if(is_null($user->PasswordEncrypt)) {
                $user->password = '**********';
            } else {
                $user->password = Crypt::decryptString($user->PasswordEncrypt);
            }
        }
        return response()->json([
            'status' => 'Ok',
            'users' => $users
        ], 200);
    }

    /**
     * Obtener roles de usuarios
     */
    public function getRoles()
    {
        $roles = $this->user_repository->getRoles();
        return response()->json([
            'status' => 'Ok',
            'roles'  => $roles
        ], 200);
    }

    /**
     * Guardar rol de usuario
     */
    public function saveRoles()
    {
        try {
            $this->accesos_db->beginTransaction();
            // Validar que el nombre del rol no sea repetido
            $roles = $this->user_repository->getRoles();
            for($i = 0; $i < count($roles); $i++) {
                $nombreRol = mb_strtoupper($roles[$i]->NombreRol, 'UTF-8');
                $nombreRolRequest = mb_strtoupper(request()->name, 'UTF-8');
                if($nombreRol == $nombreRolRequest) {
                    throw New Exception("EL ROL INGRESADO YA EXISTE");
                }
            }
            $this->accesos_db->table('osaf_tipo_usuario_cat')->insert([
                'NombreRol'    => mb_strtoupper(request()->name, 'UTF-8'),
                'PlataformaID' => request()->platformId,
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s')
            ]);
            $this->accesos_db->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->accesos_db->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtiene el tipo de usuarios
     */
    public function getTipoUsuarios()
    {
        $plataforma = $this->accesos_db->table('osaf_plataforma_cat')
            ->where('Descripcion', '=', request()->platform)
            ->first();
        $tipos = $this->accesos_db->table('osaf_tipo_usuario_cat')
            ->where('PlataformaID', $plataforma->PlataformaID)
            ->where(function($query) {
                $query->where('NombreRol', 'not like', '%Enlace%');
            })
            ->get();
        return response()->json([
           'status'            => 'Ok',
           'tipoUsuariosRoles' => $tipos
        ]);
    }

    /**
     * Obtener la plataforma
     */
    public function getPlatform()
    {
        $plataforma = $this->accesos_db->table('osaf_plataforma_cat')
            ->where('Descripcion', '=', request()->platform)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'plataforma' => $plataforma
        ], 200);
    }

    public function getUsersPrivilegios()
    {
        $users = request()->users;
        $plataformaId = request()->plataformaId;
        $tipoUsuarioRol = request()->tipoUsuarioRol;
        $accesos = [];
        for($i = 0; $i < count($tipoUsuarioRol); $i++) {
            $idRol = $tipoUsuarioRol[$i]['TipoUsuarioID'];
            $accesos['tipo_usuario_'.$idRol] = [];
            for($j = 0; $j < count($users); $j++) {
                $privilegios = $this->accesos_db->select('call sp_obtenerTipoUsuarioPlataforma(?, ?)', [$users[$j]['UsuarioID'], $plataformaId]);
                if(count($privilegios) > 0) {
                    for($k = 0; $k < count($privilegios); $k++) {
                        if($privilegios[$k]->TipoUsuarioID == $idRol) {
                            array_push($accesos['tipo_usuario_'.$idRol], $users[$j]['UsuarioID']);
                        }
                    }
                }
            }
        }
        return response()->json([
            'status'     => 'Ok',
            'privilegio' => $accesos
        ], 200);
    }

    /**
     * Agregrar privilegio a usuarios
     */
    public function addUserPrivilegio()
    {
        try {
            $this->accesos_db->select('call sp_asignarPrivilegiosUsuario(?, ?, ?)', [request()->user, request()->plataforma, request()->tipo]);
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error'
            ], 500);
        }
    }

    /**
     * Remover privilegio a usuarios
     */
    public function removeUserPrivilegio()
    {
        try {
            $this->accesos_db->beginTransaction();
            $this->accesos_db->select('call sp_quitarPrivilegiosUsuario(?, ?, ?)', [request()->user, request()->plataforma, request()->tipo]);
            $this->accesos_db->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->accesos_db->rollBack();
            return response()->json([
                'status' => 'error'
            ], 500);
        }
    }

    /**
     * Actualizar contraseña de usuario
    */
    public function updatePassword(){
        $password = request()->password;

        $password_hash = Hash::make($password);

        $password_encrypt = Crypt::encryptString($password);

        try {
            $this->main_db->beginTransaction();
            $this->main_db->table('osaf_usuarios')
                ->where('UsuarioID', request()->usuario_id)
                ->update(['Password'=>$password_hash, 'PasswordEncrypt'=>$password_encrypt, 'updated_at' => date('Y-m-d H:i:s')]);
            $this->main_db->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch(Exception $e){
            $this->main_db->rollBack();
            return response()->json([
                'status' => 'error'
            ], 500);
        }
    }
}