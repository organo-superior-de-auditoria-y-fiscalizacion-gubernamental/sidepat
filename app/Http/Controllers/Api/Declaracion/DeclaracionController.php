<?php

namespace App\Http\Controllers\Api\Declaracion;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Declaracion\DeclaracionRepository;
use App\Mail\NotificationDeclare;
use Carbon\Carbon;
use Illuminate\Support\Facades\{DB, Log, Mail};
use Exception;
use Illuminate\Support\Arr;

class DeclaracionController extends Controller{

    protected $dr; //Repositorio de declaracion
    protected $db_declaracion;

    public function __construct(DeclaracionRepository $dr){
        $this->dr = $dr;
        $this->db_declaracion = DB::connection('declaracion');
    }

    /**
     * Obtener el tipo de declaración patrimonial, ejercicio de la declaración y modulos de la declaracion
     */
    public function getDataDeclaracion()
    {
        $user               = request()->user();
        $data['ejercicio']  = $this->dr->getEjercicioDeclaracion();
        $data['tipo']       = $this->dr->getTipoDeclaracion($user->UsuarioID, $data['ejercicio']->EjercicioID);
        $data['inf_gral']   = $this->dr->getDeclaracionData($user->UsuarioID, $data['ejercicio']->EjercicioID);
        $data['secciones']  = $this->dr->getSeccionesADeclarar($user->UsuarioID, $data['ejercicio']->EjercicioID);
        return response()->json([
            'status'      => 'Ok',
            'declaracion' => $data
        ], 200);
    }

    /**
     * Obtener usuarios a declarar
     */
    public function getUsersToDeclare()
    {
        $ejercicio = $this->dr->getEjercicioDeclaracion();
        $users     = $this->dr->getUsuariosDeclaracion($ejercicio->EjercicioID);
        return response()->json([
            'status' => 'Ok',
            'users'  => $users
        ], 200);
    }

    /**
     * Obtener tipos de declaracion
     */
    public function getTiposDeclaracion()
    {
        $tipos_declaracion = $this->dr->getTiposDeclaracion();
        return response()->json([
            'status'            => 'Ok',
            'tiposDeclaracion' => $tipos_declaracion
        ], 200);
    }

    /**
     * Asignar a usuarios a una declaración en especifico
     */
    public function asignarUsuariosDeclaracion()
    {
        $usuarios  = request('usuarios');
        $tipo      = request('tipo');
        $ejercicio = $this->dr->getEjercicioDeclaracion();
        for($i = 0; $i < count($usuarios); $i++) {
            $this->db_declaracion->select('call sp_crearDeclaracion(?, ?, ?)', [$usuarios[$i], $ejercicio->EjercicioID, $tipo]);
        }
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    /**
     * Remover a usuarios a una declaración en especifico
     */
    public function removerUsuariosDeclaracion()
    {
        $usuario = request()->user;
        $ejercicio = $this->dr->getEjercicioDeclaracion();
        $declaracion = $this->dr->getDeclaracionByUser($usuario, $ejercicio->EjercicioID);
        $this->db_declaracion->select('call sp_baja_declaracion(?)', [$declaracion->DeclaracionID]);
        return response()->json([
            'status' => 'Ok',
        ], 200);
    }

    public function getDatosPersonales()
    {
        $user = request()->user();
        $datos =  $this->dr->getDatosPersonalesMain($user->UsuarioID);
        return response()->json([
            'status'    => 'Ok',
            'datos'     => $datos
        ], 200);
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatalogoEstadoCivil()
    {
        $estado_civil_cat = $this->dr->getCatalogoEstadoCivil();
        return response()->json([
            'status'    => 'Ok',
            'catalogo'     => $estado_civil_cat
        ], 200);
    }

    /**
     * Guardar datos personales
     */
    public function saveInformacionPersonal(){
        try {
            $this->db_declaracion->beginTransaction();
            $resultado = $this->dr->registrarDatosPersonales(request()->info);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDIERON REGISTRAR LOS DATOS PERSONALES");
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener estados
     */
    public function getEstados()
    {
        return response()->json([
            'status'  => 'Ok',
            'estados' =>  $this->dr->getEstados()
        ], 200);
    }

    /**
     * Obtener tipos de declaracion
     */
    public function getNivelesEstudio()
    {
        $niveles = $this->dr->getCatalogoNivelesEstudio();
        return response()->json([
            'status'            => 'Ok',
            'niveles' => $niveles
        ], 200);
    }

    /**
     * Obtener municipios del estado
     */
    public function getMunicipios()
    {
        return response()->json([
            'status'     => 'Ok',
            'municipios' => $this->dr->getMunicipios(request('estado_id'))
        ], 200);
    }

    /**
     * Guardar la direccion a la declaracion
     */
    public function saveDireccion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            // Actualizar direccion a la declaracion patrimonial
            $this->db_declaracion
                ->table('oic_declaraciones_patrimoniales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'DireccionID' => $resultado[0]->direccion_id
                ]);
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 2);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener direccion
     */
    public function getDireccion()
    {
        $direccion = $this->db_declaracion->table('oic_direcciones')
            ->where('DireccionID', '=', request('direcion_id'))
            ->first();
        return response()->json([
            'status'    => 'Ok',
            'direccion' => $direccion
        ], 200);
    }

    /**
     * Guardar los datos curriculares del declarante
     */
    public function saveDatosCurriculares()
    {
        $user = request()->user()->UsuarioID;
        try {
            $resultado = $this->dr->registrarDatosCurriculares(request()->info, $user);
            if ($resultado === false) {
                throw new Exception("ERROR | NO SE PUDIERON REGISTRAR LOS DATOS PERSONALES");
            }
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatOrdenGobierno()
    {
        $orden_gobierno_cat = $this->dr->getCatalogoOrdenGobierno();
        return response()->json([
            'status'    => 'Ok',
            'catalogo_orden'     => $orden_gobierno_cat
        ], 200);
    }

    /**
     * Obtener catalogo de ambito publico
     */
    public function getCatAmbitoPublico()
    {
        $ambito_publico_cat = $this->dr->getCatalogoAmbitoPublico();
        return response()->json([
            'status'    => 'Ok',
            'catalogo_ambito'     => $ambito_publico_cat
        ], 200);
    }

    /**
     * Obtener catalogo de sector laboral
     */
    public function getCatSectorLaboral()
    {
        $sector_cat = $this->dr->getCatalogoSectorLaboral();
        return response()->json([
            'status' => 'Ok',
            'catalogo_sector' => $sector_cat
        ], 200);
    }

    /**
     * Guardar experiencia laboral
     */
    public function saveExpLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarExpLaboral(request()->info, $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener datos para seccion de cargo
     */
    public function getDataCargo()
    {
        return response()->json([
            'estados'           => $this->dr->getEstados(),
            'areas_adscripcion' => $this->dr->getAreasAdscripcion(),
            'niveles_cargo'     => $this->dr->getNivelesCargo(),
            'ordenes_gobierno'  => $this->dr->getOrdenesGobierno(),
            'ambitos_publicos'  => $this->dr->getAmbitosPublicos(),
            'direccion'         => $this->dr->getDireccionOsafig()
        ]);
    }

    /**
     * Cargar experiencias laborales
     */
    public function getExperienciasLaborales()
    {
        $usuario = request()->user()->UsuarioID;
        $experiencias = $this->dr->getExperienciasLaborales($usuario);
        return response()->json([
            'status' => 'Ok',
            'experiencias' => $experiencias
        ]);
    }

    /**
     * Guardar datos del cargo
     */
    public function saveDatosCargo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario   = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarDatosCargo(request()->all(), $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            $this->db_declaracion->commit();
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar experiencia laboral como oculta
     */
    public function borrarExperienciaLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_experiencia_laboral_detalle')
                ->where('ExperienciaLaboralDetalleID', '=', request()->ExperienciaLaboralDetalleID)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener datos curriculares de una auditoria dada
     */
    public function getDataCurriculares()
    {
        $curriculares =$this->db_declaracion->table('oic_nivel_estudios_detalle')
            ->where('DeclaracionID', '=', request()->declaracionID)
            ->first();
        return response()->json([
            'status'       => 'Ok',
            'curriculares' => $curriculares
        ], 200);
    }

    /**
     * Obtener el cargo registrado en caso de existir
     */
    public function getCargoRegistrado()
    {
        $cargo = $this->db_declaracion->table('oic_cargos')
            ->where('DeclaracionID', '=', request()->declaracion_id)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'cargo'  => $cargo
        ], 200);
    }

    /**
     * Marcar experiencia laboral como ninguino
     */
    public function marcarNungunoExperienciaLaboral()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $experiencias = $this->dr->getExperienciasLaborales($usuario);
            foreach($experiencias as $experiencia) {
                $this->db_declaracion->table('oic_experiencia_laboral_detalle')
                    ->where('ExperienciaLaboralDetalleID', '=', $experiencia->ExperienciaLaboralDetalleID)
                    ->update([
                        'Oculto'     => true,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 5);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo de instrumentos financieros
     */
    public function getInstrumentosFinancieros()
    {
        $instrumentos_cat = $this->dr->getCatalogoInstrumentos();
        return response()->json([
            'status'    => 'Ok',
            'catalogo'     => $instrumentos_cat
        ], 200);
    }

    /**
     * registrar ingresos de la declaracion
     */
    public function saveIngresos()
    {
        $ingresos = request()->all();
        try{
            //Registro de los ingresos principal
            $this->db_declaracion->beginTransaction();
            $usuario   = request()->user()->UsuarioID;
            $declaracion_ingreso = $this->db_declaracion->table('oic_ingresos')
                ->where('DeclaracionID', '=', $ingresos["declaracion_id"])
                ->first();
            if(is_null($declaracion_ingreso)) {
                $ingreso_principal = $this->db_declaracion->table('oic_ingresos')
                    ->insertGetId([
                        'RemuneracionMensualNeta'   => $ingresos["punto_1"],
                        'TotalOtrosIngresos'        => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones'             => $ingresos["observaciones"],
                        'DeclaracionID'             => $ingresos["declaracion_id"],
                        'created_at'                => Carbon::now(),
                        'updated_at'                => Carbon::now(),
                        'UsuarioID'                 => $usuario
                    ]);
            } else {
                $ingreso_principal = $declaracion_ingreso->IngresoID;
                $this->db_declaracion->table('oic_ingresos')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->update([
                        'RemuneracionMensualNeta'   => $ingresos["punto_1"],
                        'TotalOtrosIngresos'        => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones'             => $ingresos["observaciones"],
                        'updated_at'                => Carbon::now(),
                    ]);
            }
            // ----------------------------------
            //      INGRESOS POR ACTIVIDAD
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_actividad = [];
            foreach($ingresos["actividad_industrial"] as $ingreso){
                $data = [
                    'NombreRazonSocial' => $ingreso["nombre"],
                    'TipoNegocio'       => $ingreso["tipo"],
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                    'IngresoPasado'     => 0,
                    'IngresoID'         => $ingreso_principal,
                    'Monto'             => $ingreso["monto"]
                ];
                array_push($ingresos_actividad, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_indutrial')
                ->insert($ingresos_actividad);

            // ----------------------------------
            //  INGRESOS POR ACTIVIDAD FINANCIERA
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_financiera = [];
            $instrumentos = $this->db_declaracion
                ->table('oic_tipo_instrumento_cat')
                ->get();
            $instrumentosId = Arr::pluck($instrumentos, 'IntrumentoID');
            foreach($ingresos["actividad_financiera"] as $ingreso){
                $instrumentoId = (int) $ingreso["nombre"];
                if (in_array($instrumentoId, $instrumentosId)) {
                    $otroInstrumento = "";
                } else {
                    $instrumentoId   = 99;
                    $otroInstrumento = $ingreso["nombre"];
                }
                $data = [
                    'InstrumentoID'     => $instrumentoId,
                    'Monto'             => $ingreso["monto"],
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                    'IngresoPasado'     => 0,
                    'IngresoID'         => $ingreso_principal,
                    'OtroInstrumento'   => $otroInstrumento,
                ];
                array_push($ingresos_financiera, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_financiera')
                ->insert($ingresos_financiera);

            // ----------------------------------
            //  INGRESOS POR SERVICIOS PRESTADOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_servicios')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_servicios')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_servicios = [];
            foreach($ingresos["servicios_prestados"] as $ingreso){
                $data = [
                    'TipoServicioPrestado' => $ingreso["nombre"],
                    'Monto'                => $ingreso["monto"],
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                    'IngresoPasado'        => 0,
                    'IngresoID'            => $ingreso_principal
                ];
                array_push($ingresos_servicios, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_servicios')
                ->insert($ingresos_servicios);

            // ----------------------------------
            //      INGRESOS ENAJENACION
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_enajenacion = [];
            foreach($ingresos["ingresos_enajenacion"] as $ingreso){
                $data = [
                    'Monto'         => $ingreso["monto"],
                    'TipoBien'      => $ingreso["nombre"],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID'     => $ingreso_principal,
                ];
                array_push($ingresos_enajenacion, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_por_enajenacion')
                ->insert($ingresos_enajenacion);

            // ----------------------------------
            //      OTROS INGRESOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_extra')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if(count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_extra')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $otros_ingresos = [];
            foreach($ingresos["otros_ingresos"] as $ingreso){
                $data = [
                    'TipoBien'      => $ingreso["nombre"],
                    'Monto'         => $ingreso["monto"],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID'     => $ingreso_principal
                ];
                array_push($otros_ingresos, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_extra')
                ->insert($otros_ingresos);
            // marcar seccion como terminada
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada($ingresos["declaracion_id"], 8);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar seccion experiencia como terminada
     */
    public function marcarExperieinciaTerminada()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 5);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Obtener ingresos de la declaración
     */
    public function getIngresosDeclaracion()
    {
        // Obtener datos de ingreso
        $data['ingresos']                                   = $this->dr->getIngresosDeclaracion(request()->declaracion_id);
        // actividades industriales
        $data['otros_ingresos']['actividades_industriales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesIndustriales($data['ingresos']->IngresoID);
        // actividades financieras
        $data['otros_ingresos']['actividades_financieras']  = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesFinancieras($data['ingresos']->IngresoID);
        // servicios profesionales
        $data['otros_ingresos']['servicios_profesionales']  = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosServiciosProfesionales($data['ingresos']->IngresoID);
        // otros ingresos
        $data['otros_ingresos']['otros_ingresos']           = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosOtros($data['ingresos']->IngresoID);
        // bienes enajenados
        $data['otros_ingresos']['bienes_enajenados']        = (is_null($data['ingresos'])) ? [] : $this->dr->getBienesEnajenados($data['ingresos']->IngresoID);
        return response()->json([
            'status'              => 'Ok',
            'ingresosDeclaracion' => $data
        ], 200);
    }

    /**
     * Obtener catalogo de bien inmueble
     */
    public function getCatBienesInmuebles()
    {
        $bien_inmueble_cat = $this->dr->getCatalogoBienesInmueble();
        return response()->json([
            'status' => 'Ok',
            'catalogo_bien_inmueble' => $bien_inmueble_cat
        ], 200);
    }

    /**
     * Obtener catalogo de tipos de titulares
     */
    public function getCatTipoTitulares()
    {
        $tipo_titular_cat = $this->dr->getCatalogoTipoTitulares();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_titular' => $tipo_titular_cat
        ], 200);
    }

    /**
     * Obtener catalogo de tipo de titulares
     */
    public function getCatalogoTipoTitulares(){
        $catalogo = $this->db_declaracion
            ->table('oic_titulares_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatBienesMueble()
    {
        $bienes_mueble_cat = $this->dr->getCatalogoBienesMueble();
        return response()->json([
            'status' => 'Ok',
            'catalogo_bienes_mueble' => $bienes_mueble_cat
        ], 200);
    }

    /**
     * Obtener catalogo de formas de adquisicion
     */
    public function getCatFormaAdquisicion()
    {
        $forma_adquisicion_cat = $this->dr->getCatalogoTipoAdquisicion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_forma_adquisicion' => $forma_adquisicion_cat
        ], 200);
    }

    /**
     * Obtener catalogo de vinculos personales
     */
    public function getCatVinculoPersonal()
    {
        $vinculo_personal_cat = $this->dr->getCatalogoVinculoPersonal();
        return response()->json([
            'status' => 'Ok',
            'catalogo_vinculo_personal' => $vinculo_personal_cat
        ], 200);
    }

    /**
     * Registro de servidor público pasado
     */
    public function saveServidorPasado()
    {
        $ingresos = request()->all();
        try {
            //Registro de los ingresos principal
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $declaracion_ingreso = $this->db_declaracion
                ->table('oic_servidor_pasado')
                ->where('DeclaracionID', '=', $ingresos["declaracion_id"])
                ->where('Oculto', '=', false)
                ->first();
            if (is_null($declaracion_ingreso)) {
                $ingreso_principal = $this->db_declaracion->table('oic_servidor_pasado')
                    ->insertGetId([
                        'FechaInicio' => $ingresos["fecha_inicio"],
                        'FechaConclusion' => $ingresos["fecha_fin"],
                        'RemuneracionNeta' => $ingresos["punto_1"],
                        'TotalOtrosIngresos' => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones' => $ingresos["observaciones"],
                        'DeclaracionID' => $ingresos["declaracion_id"],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'SeDesempeño' => $ingresos['aplica'],
                        'UsuarioID' => $usuario
                    ]);
            } else {
                $ingreso_principal = $declaracion_ingreso->ServidorPasadoID;
                $this->db_declaracion->table('oic_servidor_pasado')
                    ->where('ServidorPasadoID', '=', $ingreso_principal)
                    ->update([
                        'FechaInicio' => $ingresos["fecha_inicio"],
                        'FechaConclusion' => $ingresos["fecha_fin"],
                        'RemuneracionNeta' => $ingresos["punto_1"],
                        'TotalOtrosIngresos' => $ingresos["punto_2"],
                        'IngNetoParejaDependientes' => $ingresos["punto_b"],
                        'Observaciones' => $ingresos["observaciones"],
                        'updated_at' => Carbon::now(),
                    ]);
            }
            // ----------------------------------
            //      INGRESOS POR ACTIVIDAD
            // ----------------------------------
            //Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_indutrial')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_actividad = [];
            foreach ($ingresos["actividad_industrial"] as $ingreso) {
                $data = [
                    'NombreRazonSocial' => $ingreso["nombre"],
                    'TipoNegocio' => $ingreso["tipo"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 1,
                    'IngresoID' => $ingreso_principal,
                    'Monto' => $ingreso["tipo"]
                ];
                array_push($ingresos_actividad, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_indutrial')
                ->insert($ingresos_actividad);

            // ----------------------------------
            //  INGRESOS POR ACTIVIDAD FINANCIERA
            // ----------------------------------
            // Borrar en caso de que ya cuente con ingresos por actividades
            $actividades = $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_actividad_financiera')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_financiera = [];
            $instrumentos = $this->db_declaracion
                ->table('oic_tipo_instrumento_cat')
                ->get();
            $instrumentosId = Arr::pluck($instrumentos, 'IntrumentoID');
            foreach ($ingresos["actividad_financiera"] as $ingreso) {
                $instrumentoId = (int)$ingreso["nombre"];
                if (in_array($instrumentoId, $instrumentosId)) {
                    $otroInstrumento = "";
                } else {
                    $instrumentoId = 99;
                    $otroInstrumento = $ingreso["nombre"];
                }
                $data = [
                    'InstrumentoID' => $instrumentoId,
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal,
                    'OtroInstrumento' => $otroInstrumento,
                ];
                array_push($ingresos_financiera, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_actividad_financiera')
                ->insert($ingresos_financiera);

            // ----------------------------------
            //  INGRESOS POR SERVICIOS PRESTADOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_servicios')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_servicios')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_servicios = [];
            foreach ($ingresos["servicios_prestados"] as $ingreso) {
                $data = [
                    'TipoServicioPrestado' => $ingreso["nombre"],
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal
                ];
                array_push($ingresos_servicios, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_servicios')
                ->insert($ingresos_servicios);

            // ----------------------------------
            //      INGRESOS ENAJENACION
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_por_enajenacion')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $ingresos_enajenacion = [];
            foreach ($ingresos["bienes_enajenados"] as $ingreso) {
                $data = [
                    'Monto' => $ingreso["monto"],
                    'TipoBien' => $ingreso["nombre"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal,
                ];
                array_push($ingresos_enajenacion, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_por_enajenacion')
                ->insert($ingresos_enajenacion);

            // ----------------------------------
            //      OTROS INGRESOS
            // ----------------------------------
            $actividades = $this->db_declaracion->table('oic_ingresos_extra')
                ->where('IngresoID', '=', $ingreso_principal)
                ->get();
            if (count($actividades) > 0) {
                $this->db_declaracion->table('oic_ingresos_extra')
                    ->where('IngresoID', '=', $ingreso_principal)
                    ->delete();
            }
            $otros_ingresos = [];
            foreach ($ingresos["otros_ingresos"] as $ingreso) {
                $data = [
                    'TipoBien' => $ingreso["nombre"],
                    'Monto' => $ingreso["monto"],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'IngresoPasado' => 0,
                    'IngresoID' => $ingreso_principal
                ];
                array_push($otros_ingresos, $data);
            }
            $this->db_declaracion
                ->table('oic_ingresos_extra')
                ->insert($otros_ingresos);
            // marcar seccion como terminada
            // Actualizar el estado de la declaracion
            $this->dr->marcarSeccionTerminada($ingresos["declaracion_id"], 9);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Guardar bien inmueble
     */
    public function saveBienInmueble()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
            if($resultado === false) {
                throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
            }
            $id = (array) $resultado[0];
            $direccion_id = $id["direccion_id"];
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('call sp_registrarBienInmueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                $usuario,
                request()->tipo_bien_id,
                request()->otro_tipo_bien,
                request()->titular,
                request()->porcentaje_propiedad,
                request()->superficie_terreno,
                request()->superficie_construida,
                request()->tipo_persona_tercero,
                request()->nombre_tercero,
                request()->rfc_tercero,
                request()->forma_adquisicion,
                request()->forma_pago,
                request()->tipo_persona_transmisor,
                request()->nombre_transmisor,
                request()->rfc_transmisor,
                request()->vinculo_transmisor,
                request()->valor,
                request()->tipo_moneda,
                request()->fecha_operacion,
                request()->valor_conforme_a,
                request()->folio_real,
                $direccion_id,
                request()->motivo_baja,
                request()->observaciones,
                request()->bien_inmueble
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatVehiculos()
    {
        $vehiculos_cat = $this->dr->getCatalogoVehiculos();
        return response()->json([
            'status' => 'Ok',
            'catalogo_vehiculos' => $vehiculos_cat
        ], 200);
    }

    /**
     * Registrar vehiculo
     */
    public function saveVehiculo(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('call sp_registrarBienMueble(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                request()->tipo_bien_id,
                request()->titular,
                request()->tipo_persona_transmisor,
                request()->nombre_razon_social,
                request()->rfc_transmisor,
                request()->relacion_transmisor,
                request()->nombre_tercero,
                request()->tipo_persona_tercero,
                request()->rfc_tercero,
                request()->forma_adquisiscion_id,
                request()->forma_pago,
                request()->valor,
                request()->tipo_moneda,
                request()->fecha_operacion,
                request()->caracteristicas,
                request()->vehiculo_marca,
                request()->vehiculo_modelo,
                request()->vehiculo_no_serie,
                request()->en_extranjero,
                request()->vehiculo_anio,
                request()->motivo_baja,
                request()->descripcion_bien,
                request()->ubicacion,
                request()->otro_tipo_bien,
                request()->bien_mueble_id,
                request()->observaciones,
                $usuario,
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener bienes inmuebles
     */
    public function getBienesInmuebles()
    {
        $usuario = request()->user()->UsuarioID;
        $bienes_inmuebles = $this->dr->getBienesInmueble($usuario);
        return response()->json([
            'status' => 'Ok',
            'bienes_inmuebles' => $bienes_inmuebles
        ]);
    }

    /**
     * Obtener Vehiculos
     */
    public function getVehiculos()
    {
        $usuario = request()->user()->UsuarioID;
        $vehiculos = $this->dr->getVehiculos($usuario);
        return response()->json([
            'status' => 'Ok',
            'vehiculos' => $vehiculos
        ]);
    }

    /**
     * Borrar el bien inmueble
     */
    public function borrarBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_inmubles')
                ->where('BienInmuebleID', '=', request()->bien_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Borrar bien mueble
     */
    public function borrarBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('BienMuebleID', '=', request()->bien_mueble_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * marcar servidor pasado como no aplica
     */
    public function saveServidorPasadoNoAplica()
    {
        try {
            $this->db_declaracion->beginTransaction();
            //verificar si existe un registro en oic_servidor_pasado
            $servidor_pasado = $this->db_declaracion
                ->table('oic_servidor_pasado')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->where('Oculto', '=', false)
                ->first();
            if (is_null($servidor_pasado)) {
                $this->db_declaracion->table('oic_servidor_pasado')
                    ->insert([
                        'DeclaracionID' => request()->declaracion_id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'seDesempeño' => false,
                        'Oculto' => true,
                        'UsuarioID' => request()->user()->UsuarioID
                    ]);
            } else {
                $this->db_declaracion
                    ->table('oic_servidor_pasado')
                    ->where('DeclaracionID', '=', request()->declaracion_id)
                    ->where('Oculto', '=', false)
                    ->update([
                        'Oculto' => true,
                        'updated_at' => Carbon::now()
                    ]);
            }
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 9);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion de vehiculos como terminadas
     */
    public function marcarTerminadaVehiculos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 11);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como ninguna
     */
    public function marcarNingunaBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_inmubles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 10);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener ingresos de la declaración servicios pasados
     */
    public function getIngresosDeclaracionServidorPasado()
    {
        // Obtener datos de ingreso
        $data['ingresos'] = $this->dr->getIngresosDeclaracionServidorPasado(request()->declaracion_id);
        // Obtener datos de ingreso pasados
        $data['ingresos_ocultos'] = count($this->dr->getIngresosDeclaracionServidorPasadoOcultos(request()->declaracion_id));
        // actividades industriales
        $data['otros_ingresos']['actividades_industriales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesIndustriales($data['ingresos']->ServidorPasadoID);
        // actividades financieras
        $data['otros_ingresos']['actividades_financieras'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosActividadesFinancieras($data['ingresos']->ServidorPasadoID);
        // servicios profesionales
        $data['otros_ingresos']['servicios_profesionales'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosServiciosProfesionales($data['ingresos']->ServidorPasadoID);
        // otros ingresos
        $data['otros_ingresos']['otros_ingresos'] = (is_null($data['ingresos'])) ? [] : $this->dr->getIngresosOtros($data['ingresos']->ServidorPasadoID);
        // bienes enajenados
        $data['otros_ingresos']['bienes_enajenados'] = (is_null($data['ingresos'])) ? [] : $this->dr->getBienesEnajenados($data['ingresos']->ServidorPasadoID);
        return response()->json([
            'status' => 'Ok',
            'ingresosDeclaracion' => $data
        ], 200);
    }

    /**
     * Marcar sección como terminada
     */
    public function marcarTerminadaBienInmueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 10);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion como ninguna
     */
    public function marcarNingunaVehiculos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 11);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener bienes muebles
     */
    public function getBienesMueble()
    {
        $usuario = request()->user()->UsuarioID;
        $bienes_mueble = $this->dr->getBienesMueble($usuario);
        return response()->json([
            'status' => 'Ok',
            'bienes_mueble' => $bienes_mueble
        ]);
    }

    /**
     * Marcar seccion de buen mueble como terminada
     */
    public function marcarTerminadaBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 12);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar ninguna bien mueble
     */
    public function marcarNingunaBienMueble()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_bienes_muebles')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 12);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * obtener catalogo de tipo de inversion
     */
    public function getCatInversion()
    {
        $tipo_inversion_cat = $this->dr->getCatalogoTipoInversion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_inversion' => $tipo_inversion_cat
        ], 200);
    }

    /**
     * obtener catalogo de tipo de inversion
     */
    public function getCatActivos()
    {
        $activos_cat = $this->dr->getCatalogoActivos(request()->id_inversion);
        return response()->json([
            'status' => 'Ok',
            'catalogo_activos' => $activos_cat
        ], 200);
    }

    /**
     * Registrar inversion
     */
    public function saveInversion(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro del inmueble
            $this->db_declaracion->select('CALL sp_registrarInversion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                request()->tipo_inversion_id,
                request()->activo,
                request()->tipo_titular,
                request()->tipo_persona_tercero,
                request()->nombre_tercero,
                request()->rfc_tercero,
                request()->numero_contrato,
                request()->es_extranjero,
                request()->ubicacion,
                request()->institucion,
                request()->rfc_institucion,
                request()->saldo,
                request()->moneda,
                request()->aclaraciones,
                request()->recurso_financiero_id,
                $usuario,
            ]);

            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener inversiones
     */
    public function getInversiones()
    {
        $usuario = request()->user()->UsuarioID;
        $inversiones = $this->dr->getInversiones($usuario);
        return response()->json([
            'status' => 'Ok',
            'inversiones' => $inversiones
        ]);
    }

    /**
     * Borrar Inversión
     */
    public function borrarInversion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_recursos_financieros')
                ->where('RecursoFinancieroID', '=', request()->recurso_financiero_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar inversiones como terminada
     */
    public function marcarTerminadaInversiones()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 13);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * marcar ninguna inversion
     */
    public function marcarNingunaInversion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_recursos_financieros')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 13);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Borrar Adeudo
     */
    public function borrarAdeudo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_adeudos')
                ->where('AdeudoID', '=', request()->adeudo_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar adeudos como terminada
     */
    public function marcarTerminadaAdeudos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 14);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * marcar ninguna adeudos
     */
    public function marcarNingunaAdeudos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_adeudos')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 14);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener prestamos
     */
    public function getPrestamos()
    {
        $usuario = request()->user()->UsuarioID;
        $prestamos = $this->dr->getPrestamos($usuario);
        return response()->json([
            'status' => 'Ok',
            'prestamos' => $prestamos
        ]);
    }

    /**
     * Guardar prestamo
     */
    public function savePrestamo()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            if(request()->tipo_bien == 1) {
                $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
                }
                $id = (array) $resultado[0];
                $direccion_id = $id["direccion_id"];
            } else {
                $direccion_id = null;
            }
            //registro del inmueble
            $this->db_declaracion->select('CALL sp_registrarPrestamo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->tipo_bien,
                request()->tipo_bien_id_vehiculo,
                request()->tipo_bien_id_inmueble,
                $direccion_id,
                request()->marca,
                request()->modelo,
                request()->anio,
                request()->no_serie,
                request()->en_extranjero,
                request()->pais,
                request()->entidad_federativa,
                request()->titular,
                request()->tipo_persona,
                request()->rfc,
                request()->observaciones,
                request()->prestamo_id,
                request()->otro_bien,
                request()->vinculo
            ]);

            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Borrar prestamo
     */
    public function borrarPrestamo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_prestamo_comodato')
                ->where('PrestamoID', '=', request()->prestamo_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar terminada prestamos
     */
    public function marcarTerminadaPrestamos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 15);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar como ninguna seccion de prestamos
     */
    public function marcarNingunaPrestamos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_prestamo_comodato')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 15);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     *Subir declaracion del sat
     */
    public function subirDeclaracionSat()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $declaracion_id = request()->declaracion_id;
            foreach (request()->fileToUpload as $file) {
                $dir = "/declaracion_patrimonial/{$declaracion_id}";
                $path = $file->store($dir, 'public');
                $this->db_declaracion->select('call sp_subirDeclaracionSAT(?,?,?)',[
                    $usuario,
                    $path,
                    request()->declaracion_id,
                ]);
            }
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch (Exception $e) {
            $this->$this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener declaracion sat
     */
    public function getDeclaracionSat()
    {
        $usuario = request()->user()->UsuarioID;
        $declaracion = $this->db_declaracion->table('oic_declaracion_sat')
            ->where('UsuarioID', '=', $usuario)
            ->where('Eliminado', '=', false)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'declaracion' => $declaracion
        ], 200);
    }

    /**
     * Borrar declaracion sat
     */
    public function borrarDeclaracionSat()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_declaracion_sat')
                ->where('DeclaracionSatID', '=', request()->DeclaracionSatID)
                ->update([
                    'Eliminado' => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        }catch (Exception $e) {
            $this->$this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener catalogo de sector laboral
     */
    public function getCatTipoParticipante()
    {
        $tipo_participante_cat = $this->dr->getCatalogoTipoParticipante();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_participante' => $tipo_participante_cat
        ], 200);
    }

    /**
     * Guardar participacion en empresa
     */
    public function saveParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroParticipacionEmpresas(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->nombre_empresa,
                request()->rfc_empresa,
                request()->porcentaje_participacion,
                request()->tipo_participacion,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->ubicacion,
                request()->es_extranjero,
                request()->participante,
                request()->sector_id,
                request()->observaciones,
                request()->participacion_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener participacion empresas
     */
    public function getParticipacionEmpresas()
    {
        $usuario = request()->user()->UsuarioID;
        $participacion_empresas = $this->dr->getParticipacionEmpresas($usuario);
        return response()->json([
            'status' => 'Ok',
            'participacion' => $participacion_empresas
        ]);
    }

    /**
     * borrar participacion en empresa
     */
    public function borrarParticipacionEmpresa(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_empresas')
                ->where('ParticipacionID', '=', request()->participacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada participacion empresa
     */
    public function marcarTerminadaParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 16);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en empresa
     */
    public function marcarNingunaParticipacionEmpresa()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_empresas')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 16);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo de tipo institucion
     */
    public function getCatTipoInstitucion()
    {
        $tipo_Institucion_cat = $this->dr->getCatalogoTipoInstitucion();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_institucion' => $tipo_Institucion_cat
        ], 200);
    }

    /**
     * Guardar participacion en empresa
     */
    public function saveParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroParticipacionInstitucion(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->participante,
                request()->tipo_institucion_id,
                request()->nombre_institucion,
                request()->rfc_institucion,
                request()->puesto_rol,
                request()->fecha_inicio,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->observaciones,
                request()->participacion_institucion_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener participacion institucion
     */
    public function getParticipacionInstitucion()
    {
        $usuario = request()->user()->UsuarioID;
        $participacion_institucion = $this->dr->getParticipacionInstitucion($usuario);
        return response()->json([
            'status' => 'Ok',
            'participacion' => $participacion_institucion
        ]);
    }

    /**
     * borrar participacion en institucion
     */
    public function borrarParticipacionInstitucion(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_instituciones')
                ->where('ParticipacionInstitucionesID', '=', request()->participacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada participacion institucion
     */
    public function marcarTerminadaParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 17);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en institucion
     */
    public function marcarNingunaParticipacionInstitucion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_participacion_instituciones')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 17);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener datos de dependientes - pareja
     */
    public function getDatosPreja()
    {
        $usuario = request()->user()->UsuarioID;
        $datos_pareja = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('UsuarioID', '=', $usuario)
            ->where('Pareja', '=', 1)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'datos_pareja' => $datos_pareja
        ], 200);
    }

    /**
     * Obtener datos de dependientes
     */
    public function getDatosDependientes()
    {
        $usuario = request()->user()->UsuarioID;
        $datos_pareja = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('UsuarioID', '=', $usuario)
            ->where('Pareja', '=', 0)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'datos_pareja' => $datos_pareja
        ], 200);
    }

    /**
     * Guardar dependientes
     */
    public function saveDependientes()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $direccion_id = null;
            if(request()->habita_domicilio == 0) {
                $resultado = $this->dr->registrarDireccion(request()->all(), $usuario);
                if($resultado === false) {
                    throw new Exception("ERROR | NO SE PUDO REGISTRAR LA DIRECCION");
                }
                $id = (array) $resultado[0];
                $direccion_id = $id["direccion_id"];
            }
            $actividad_laboral = null;
            //registro del inmueble
            $this->db_declaracion->select('call sp_registrarDependientes(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->nombre,
                request()->primer_apellido,
                request()->segundo_apellido,
                request()->fecha_nacimiento,
                request()->rfc,
                request()->relacion_id,
                request()->es_extrajero,
                request()->curp,
                request()->es_dependiente,
                request()->habita_domicilio,
                '',
                $direccion_id,
                $actividad_laboral,
                request()->observaciones,
                request()->is_pareja,
                request()->dependiente_id,
                $usuario,
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener la actividad laboral de la pareja
     */
    public function getExperienciasDependiente($actividad_laboral_id)
    {
        $experiencias = $this->db_declaracion->table('oic_actividad_laboral_detalle')
            ->where('ActividadLaboralDetalleID', '=', $actividad_laboral_id)
            ->where('Oculto', '=', 0)
            ->get();
        return response()->json([
            'status' => 'Ok',
            'experiencias' => $experiencias
        ]);
    }

    /**
     * Guardar experiencia laboral de los dependientes
     */
    public function saveExperienciaLaboralDependientes()
    {
        try{
            $this->db_declaracion->beginTransaction();
            $request = request()->info;
            $sector = $this->db_declaracion
                ->table('oic_sector_productivo_cat')
                ->where('Sector', '=', $request['sector_id'])
                ->first();
            $sector_id = (is_null($sector)) ? 0 : $sector->SectorID;
            $actividad_laboral = $this->db_declaracion->select('call sp_registrarActividadLaboral(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $request['experiencia_laboral_id'],
                $request['ambito_sector'],
                $request['orden_gobierno_id'],
                $request['ambito_id'],
                $request['institucion'],
                $request['area'],
                $request['puesto'],
                $request['funcion_principal'],
                $request['salario'],
                $request['fecha_ingreso'],
                $request['rfc_institucion'],
                $sector_id,
                $request['es_proveedor'],
                $request['observacion'],
            ]);
            $actividad_laboral_id = $actividad_laboral[0]->actividad_laboral_id;
            $dependiente_id       = $request['dependiente_id'];
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('DependienteID', '=', $dependiente_id)
                ->update([
                    'ActividadLaboralID' => $actividad_laboral_id,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener datos del dependiente
     */
    public function getDependiente($dependiente_id)
    {
        $dependiente = $this->db_declaracion->table('oic_usuarios_dependientes')
            ->where('DependienteID', '=', $dependiente_id)
            ->first();
        return response()->json([
            'status' => 'Ok',
            'dependiente' => $dependiente
        ], 200);
    }

    /**
     * Borrar actividad laboral
     */
    public function borrarExperienciaDependiente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('ActividadLaboralID', '=', request()->ActividadLaboralDetalleID)
                ->update([
                    'ActividadLaboralID' => null
                ]);
            $this->db_declaracion->table('oic_actividad_laboral_detalle')
                ->where('ActividadLaboralDetalleID', '=', request()->ActividadLaboralDetalleID)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    public function borrarDependiente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion->table('oic_usuarios_dependientes')
                ->where('DependienteID', '=', request()->dependiente_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de la pareja terminada
     */
    public function marcarTerminadaDatosPareja()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 6);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de pareja como ningun
     */
    public function marcarNingunaDatosPareja()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $this->db_declaracion
                ->table('oic_usuarios_dependientes')
                ->where('UsuarioID', '=', $usuario)
                ->where('Pareja', '=', 1)
                ->where('Oculto', '=', 0)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 6);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar datos de la pareja terminada
     */
    public function marcarTerminadaDatosDependientes()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 7);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch (Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Marcar datos de pareja como ningun
     */
    public function marcarNingunaDatosDependientes()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            $this->db_declaracion
                ->table('oic_usuarios_dependientes')
                ->where('UsuarioID', '=', $usuario)
                ->where('Pareja', '=', 0)
                ->where('Oculto', '=', 0)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 7);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener catalogo beneficiarios
     */
    public function getCatBeneficiarios()
    {
        $beneficiarios_cat = $this->dr->getCatalogoBeneficiarios();
        return response()->json([
            'status' => 'Ok',
            'beneficiarios_cat' => $beneficiarios_cat
        ]);
    }

    public function saveApoyoBeneficio()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroApoyos(?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->beneficiario,
                request()->nombre_programa,
                request()->nombre_institucion,
                request()->ambito_id,
                request()->tipo_apoyo,
                request()->forma_recepcion,
                request()->monto_apoyo,
                request()->especificacion,
                request()->observaciones,
                request()->apoyo_beneficio_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener apoyos y beneficios
     */
    public function getApoyosBeneficios()
    {
        $usuario = request()->user()->UsuarioID;
        $apoyos_beneficios = $this->dr->getApoyosBeneficios($usuario);
        return response()->json([
            'status' => 'Ok',
            'apoyos' => $apoyos_beneficios
        ]);
    }

    /**
     * borrar participacion apoyo o beneficio
     */
    public function borrarApoyosBeneficios(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_apoyos_beneficios')
                ->where('ApoyoBeneficioID', '=', request()->apoyo_beneficios)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada la seccion de apoyo o beneficio
     */
    public function marcarTerminadaApoyosBeneficios()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 18);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun apoyo o beneficio
     */
    public function marcarNingunaApoyosBeneficios()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_apoyos_beneficios')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 18);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Guardar representacion
     */
    public function saveRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registrarRepresentacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->representante,
                request()->tipo_representacion,
                request()->fecha_inicio,
                request()->representante_tipo_persona,
                request()->nombre_razon_social,
                request()->rfc_representante,
                request()->recibe_remuneracion,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->sector_id,
                request()->observaciones,
                request()->declaracion_id,
                $usuario,
                request()->representacion_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'error' => $e,
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener representaciones
     */
    public function getRepresentaciones()
    {
        $usuario = request()->user()->UsuarioID;
        $representaciones = $this->dr->getRepresentaciones($usuario);
        return response()->json([
            'status' => 'Ok',
            'representaciones' => $representaciones
        ]);
    }

    /**
     * borrar representacion
     */
    public function borrarRepresentacion(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_representacion')
                ->where('RepresentacionID', '=', request()->representacion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada representacion
     */
    public function marcarTerminadaRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 19);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ninguna participacion en empresa
     */
    public function marcarNingunaRepresentacion()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_representacion')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 19);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    public function saveCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroCliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->act_lucrativa_dependiente,
                request()->titular,
                request()->nombre_empresa_servicio,
                request()->rfc_empresa,
                request()->cliente_rfc,
                request()->razon_social_cliente,
                request()->sector_id,
                request()->monto_mensual,
                request()->es_extranjero,
                request()->ubicacion,
                request()->observaciones,
                request()->cliente_principal_id,
                request()->tipo_persona_cliente
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getClientes()
    {
        $usuario = request()->user()->UsuarioID;
        $clientes = $this->dr->getClientes($usuario);
        return response()->json([
            'status' => 'Ok',
            'clientes' => $clientes
        ]);
    }

    /**
     * borrar cliente
     */
    public function borrarCliente(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_clientes_principales')
                ->where('ClientePrincipalID', '=', request()->cliente_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada clientes principales
     */
    public function marcarTerminadaCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 20);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun cliente
     */
    public function marcarNingunaCliente()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_clientes_principales')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 20);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    public function saveBeneficiosPrivados()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;

            //registro de participacion
            $participacion = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroBeneficioPrivado(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                request()->declaracion_id,
                $usuario,
                request()->tipo_beneficio,
                request()->titular,
                request()->otorgante_tipo_persona,
                request()->nombre_razon_social,
                request()->rfc_otorgante,
                request()->recepcion_beneficio,
                request()->beneficio,
                request()->monto_mensual,
                request()->tipo_moneda,
                request()->sector_id,
                request()->observaciones,
                request()->beneficio_privado_id
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getBeneficios()
    {
        $usuario = request()->user()->UsuarioID;
        $beneficios_privados = $this->dr->getBeneficiosPrivados($usuario);
        return response()->json([
            'status' => 'Ok',
            'beneficios_privados' => $beneficios_privados
        ]);
    }

    /**
     * borrar beneficio privado
     */
    public function borrarBeneficioApoyo(){
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_beneficios_privados')
                ->where('BeneficioPrivadoID', '=', request()->beneficio_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección como terminada beneficios privados
     */
    public function marcarTerminadaBeneficioApoyo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 21);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar sección terminada sin ningun beneficio privado
     */
    public function marcarNingunaBeneficioApoyo()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_beneficios_privados')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 21);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Enviar correos de notificaciones
     */
    public function enviarCorreosNotificacion()
    {
        $users = request()->users;
        foreach ($users as $user) {
            try {
                Mail::to($user['Email'])->send(new NotificationDeclare($user));
            } catch (Exception $e) {
                Log::info("ERROR: NO SE PUDO MANDAR EL CORREO DE NOTIFICACION AL USUARIO {$user['Email']}");
                Log::info("DETALLE DE ERROR: {$e->getMessage()}");
            }
        }
        return response()->json([
            'status' => 'Ok'
        ], 200);
    }

    /**
     * Enviar correos de notificaciones
     */
    public function enviarCorreoNotificacion()
    {
        $user = request()->user;
        try {
            Mail::to($user['Email'])->send(new NotificationDeclare($user));
        } catch (Exception $e) {
            Log::info("ERROR: NO SE PUDO MANDAR EL CORREO DE NOTIFICACION AL USUARIO {$user['Email']}");
            Log::info("DETALLE DE ERROR: {$e->getMessage()}");
        }
    }

    /**
     * Registrar fideicomiso
     */
    public function saveFideicomiso(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            //registro de participacion
            $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registroFideicomiso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->participante,
                request()->tipo_fideicomiso,
                request()->tipo_participacion,
                request()->rfc_fideicomiso,
                request()->tipo_persona_fidecomitente,
                request()->razon_social_fideicomitente,
                request()->rfc_fideicomitente,
                request()->tipo_persona_fideicomisario,
                request()->razon_social_fideicomisario,
                request()->rfc_fideicomisario,
                request()->sector_id,
                request()->es_extranjero,
                request()->observaciones,
                request()->fideicomisos_id,
                request()->tipo_persona_fiduciario,
                request()->razon_social_fiduciario,
                request()->rfc_fiduciario
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        }catch(exception $e){
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'error' => $e,
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Obtener clientes
     */
    public function getFideicomisos()
    {
        $usuario = request()->user()->UsuarioID;
        $fideicomisos = $this->dr->getFideicomisos($usuario);
        return response()->json([
            'status' => 'Ok',
            'fideicomisos' => $fideicomisos
        ]);
    }

    /**
     * Borrar fideicomiso
     */
    public function borrarFideicomiso()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_fideicomisos')
                ->where('FideicomisosID', '=', request()->fideicomiso_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * Marcar seccion como terminada
     */
    public function marcarTerminadaFideicomisos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 22);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }

    /**
     * marcar seccion de fideicomisos como ninguna
     */
    public function marcarNingunaFideicomisos()
    {
        try {
            $this->db_declaracion->beginTransaction();
            $this->db_declaracion
                ->table('oic_fideicomisos')
                ->where('DeclaracionID', '=', request()->declaracion_id)
                ->update([
                    'Oculto'     => true,
                    'updated_at' => Carbon::now()
                ]);
            $this->dr->marcarSeccionTerminada(request()->declaracion_id, 22);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            Log::error("ERROR: {$e->getMessage()} | FILE: {$e->getFile()} | LINE: {$e->getLine()}");
            $this->db_declaracion->rollBack();
            return response()->json([
                'status' => 'error',
            ], 500);
        }
    }
    
    /**
     * obtener catalogo de tipo de adeudo
     */
    public function getCatTipoAdeudo()
    {
        $tipo_adeudo_cat = $this->dr->getCatalogoTipoAdeudo();
        return response()->json([
            'status' => 'Ok',
            'catalogo_tipo_adeudo' => $tipo_adeudo_cat
        ], 200);
    }
    /**
     * Obtener inversiones
     */
    public function getAdeudos()
    {
        $usuario = request()->user()->UsuarioID;
        $inversiones = $this->dr->getAdeudos($usuario);
        return response()->json([
            'status' => 'Ok',
            'adeudos' => $inversiones
        ]);
    }
    /**
     * Registrar adeudo
     */
    public function saveAdeudos(){
        try{
            $this->db_declaracion->beginTransaction();
            $usuario = request()->user()->UsuarioID;
            // dd(request()->all());
            //registro del inmueble
            $inmueble = $this->db_declaracion->select('CALL osafgobm_declaracion.sp_registrarAdeudo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
                $usuario,
                request()->declaracion_id,
                request()->titular,
                request()->tipo_adeudo_id,
                request()->no_cuenta,
                request()->fecha_otorgamiento,
                request()->monto_original,
                request()->moneda,
                request()->saldo_insoluto,
                request()->nombre_tercero,
                request()->tipo_persona_tercero,
                request()->rfc_tercero,
                request()->ubicacion,
                request()->nombre_razon_social,
                request()->tipo_persona_declarante,
                request()->es_extranjero,
                request()->aclaraciones,
                request()->adeudo_id,
                request()->rfc_otorgante
            ]);
            $this->db_declaracion->commit();
            return response()->json([
                'status' => 'Ok',
            ], 200);
        } catch(Exception $e) {
            $this->db_declaracion->rollBack();
            Log::error("{$e->getMessage()}");
            return response()->json([
                'status' => 'Error'
            ], 500);
        }
    }
}
