<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\{Crypt, Hash, DB, Log};
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\User;

class AuthController extends Controller
{
    public function login()
    {
        try {
            $user = User::where('Email', request('email'))->first();
            if ( $user ) {
                // Verificar si la contraseña es válida
                if( Hash::check(request('password'), $user->Password) ) {
                    // TODO: ANALIZAR CAMBIOS EN EL INICIO DE LA SESIÓN EN LA PLATAFORMA
                    if(is_null($user->api_token)){
                        $user->api_token = Str::random(80);
                        $user->save();
                    }
                    Log::info("REGISTRO DE INICIO DE SESION DE USUARIO | {$user->Email}");
                    DB::select('call sp_entradaBitacora(?,?,?)', [1, $user->UsuarioID, null]);
                    return response()->json([
                        'message'    => 'Bienvenido@ ' . $user->Nombres,
                        'enlace'     => false,
                        'api_token'  => $user->api_token,
                    ], 200);
                    //Obtener el permiso de acceso a la platadorma
                    /*$sad_access_permission = DB::connection('main')
                        ->table('osaf_permisos_cat')
                        ->where('Nombre', 'acceso_al_sistema_sad')
                        ->first();
                    if($sad_access_permission) {
                        //Verificar que el usuario tiene acceso a la plataforma
                        $has_permission_to_access = DB::connection('main')
                            ->table('osaf_usuario_permiso_det')
                            ->where('UsuarioID', $user->UsuarioID)
                            ->where('PermisoID', $sad_access_permission->PermisoID)
                            ->first();
                        if($has_permission_to_access) {
                            if($user->Email === 'abuenrostro@osaf.gob.mx') {
                                $user_rol = 'responsable';
                            } else {
                                $user_rol = 'auditor';
                            }
                            if(is_null($user->api_token)){
                                $user->api_token = Str::random(80);
                                $user->save();
                            }
                            Log::info("REGISTRO DE INICIO DE SESION DE USUARIO | {$user->Email}");
                            DB::select('call sp_entradaBitacora(?,?,?)', [1, $user->UsuarioID, null]);
                            return response()->json([
                                'message'    => 'Bienvenido@ ' . $user->Nombres,
                                'enlace'     => false,
                                'api_token'  => $user->api_token,
                                'user_rol'  => $user_rol
                            ], 200);
                        }
                        throw new \Exception("EL USUARIO NO CUENTA CON PERMISOS PARA ACCEDER A LA PLATAFORMA");
                    }
                    throw new \Exception("PERMISO DE ACCESO AL SISTEMA NO DEFINIDO");*/
                } else {
                    throw new \Exception("LA CONTRASEÑA DEL USUARIO NO CONCUERDA CON NUESTROS REGISTROS");
                }
            } else {
                // ------------------------------------------------------
                // TODO: CAMBIO TEMPORAL
                // ------------------------------------------------------
                if(request()->email == 'baja01@nodomain.com' || request()->email == 'efrain.95ac1@gmail.com') {
                    $enlace = DB::connection('main')
                        ->table('osaf_v_enlaces')
                        ->where('EmailInstitucional', '=', 'efrain.95ac1@gmail.com')
                        ->first();
                } else {
                    $enlace = DB::connection('main')
                        ->table('osaf_v_enlaces')
                        ->where('EmailInstitucional', '=', request('email'))
                        ->first();
                }
                if($enlace) {
                    // Verificar si la contraseña es válida
                    $password = Crypt::decryptString($enlace->Password);
                    if($password == request('password') ) {
                        //Obtener registro del detalle del funcionario
                        $enlace_det = DB::connection('main')
                            ->table('osaf_entidades_funcionarios_det')
                            ->where('FuncionarioID', '=', $enlace->FuncionarioID)
                            ->first();
                        if(is_null($enlace_det->api_token)){
                            DB::connection('main')
                                ->table('osaf_entidades_funcionarios_det')
                                ->where('FuncionarioID', '=', $enlace_det->FuncionarioID)
                                ->update([
                                    'api_token' => Str::random(80)
                                ]);
                        }
                        $enlace_det2 = DB::connection('main')
                            ->table('osaf_entidades_funcionarios_det')
                            ->where('FuncionarioID', '=', $enlace->FuncionarioID)
                            ->first();
                        Log::info("REGISTRO DE INICIO DE SESION DE ENLACE | {$enlace->NombreCompleto} | {$enlace->EmailInstitucional} | {$enlace->Entidad}");
                        DB::select('call sp_entradaBitacora(?,?,?)', [1, null, $enlace->FuncionarioID]);
                        return response()->json([
                            'message'    => 'Bienvenido@ ' . $enlace_det2->Email,
                            'enlace'     => true,
                            'api_token'  => $enlace_det2->api_token
                        ], 200);
                    } else {
                        throw new \Exception("LA CONTRASEÑA DEL USUARIO ENLACE NO CONCUERDA CON NUESTROS REGISTROS");
                    }
                } else {
                    $correo_institucional = request('email');
                    throw new \Exception("EL USUARIO ENLACE NO CONCUERDA CON NUESTROS REGISTROS | {$correo_institucional}");
                }
            }
        } catch(\Exception $e) {
            Log::error("ERROR | {$e->getMessage()}");
            return response()->json([
                'message'   => 'Tus credenciales no concuerdan con nuestros registros',
                'api_token' => null
            ], 401);
        }
    }
}
