<?php

namespace App\Http\Controllers\Api\ControlInterno;

use App\Http\Controllers\Controller;
use App\Http\Requests\ControlInterno\CreateEnlace;
use App\Http\Requests\ControlInterno\UpdateEnlace;
use App\Http\Repositories\{EjerciciosRepository, EnlacesRepository, EntidadesRepository};
use Illuminate\Support\Facades\{Crypt, Hash, Log, DB};
use Exception;
use Illuminate\Support\Str;

class EnlacesController extends Controller
{
    protected $er;  //Repositorio de datos de Ejercicios
    protected $enr;  //Repositorio de datos de Enlaces
    protected $entr; //Repositorio de datos de Entidades

    public function __construct(EjerciciosRepository $er, EnlacesRepository $enr, EntidadesRepository $entr)
    {
        $this->er = $er;
        $this->enr = $enr;
        $this->entr = $entr;
    }
    /**
     * Obtener a todos los enlaces
     */
    public function getEnlaces()
    {
        $enlaces = $this->enr->getEnlaces(request());
        return response()->json([
            'status' => 'Ok',
            'enlaces' => $enlaces
        ], 200);
    }

    /**
     * Crear un nuevo enlace
     */
    public function createEnlace(CreateEnlace $request)
    {
        try {
            DB::connection('main')->beginTransaction();
            //Validar que el correo_institucional no sea de alguien del osafig
            $correo_institucional = explode('@', $request->correo_institucional);
            if($correo_institucional[1] == 'osaf.gob.mx'){
                throw new Exception('No se puede registrar a un enlace con correo del OSAFIG', 501);
            }
            //Creamos registro de funcionario
            $FuncionarioID = DB::connection('main')
                ->table('osaf_funcionarios_cat')
                ->insertGetId([
                    'Nombres'         => mb_strtoupper($request->nombre, 'UTF-8'),
                    'PrimerApellido'  => mb_strtoupper($request->primer_apellido, 'UTF-8'),
                    'SegundoApellido' => mb_strtoupper($request->segundo_apellido, 'UTF-8'),
                    'Email'           => strtolower($request->email),
                    'Profesion'       => mb_strtoupper($request->profession, 'UTF-8'),
                    'Telefono'        => $request->telefono,
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s')
                ]);
            // Creamos detalle de funcionario
            $entidad = $this->entr->getEntidadByName($request->entidad);
            $str = mb_strtoupper(Str::random(8), 'UTF-8');
            $str_encrypt = Crypt::encryptString($str);
            DB::connection('main')
                ->table('osaf_entidades_funcionarios_det')
                ->insert([
                    'EntidadID'         => $entidad->EntidadID,
                    'FuncionarioID'     => $FuncionarioID,
                    'Email'             => strtolower($request->correo_institucional),
                    'Password'          => Hash::make($str),
                    'FechaIngreso'      => date("Y-m-d", strtotime($request->fecha_ingreso)),
                    'Puesto'            => mb_strtoupper($request->puesto, 'UTF-8'),
                    'PasswordDecrypted' => $str_encrypt,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
            DB::connection('main')->commit();
            return response()->json([
                'status'  => 'OK',
                'message' => 'Enlace registrado correctamente'
            ], 200);
        } catch(Exception $e) {
            DB::connection('main')->rollBack();
            $code = ($e->getCode() == 501) ? 501 : 500;
            Log::error("ERROR: NO SE PUDO REGISTRAR AL NUEVO FUNCIONARIO | DETAIL: {$e->getMessage()}");
            return response()->json([
                'status'  => 'Error',
                'message' => 'No se pudo registrar al enlace'
            ], $code);
        }
    }

    /**
     * Obtener información del enlace
     */
    public function getEnlace()
    {
        try {
            $data_enlace = $this->enr->getEnlace(request());
            if(is_null($data_enlace) == false){
                return response()->json([
                    'status' => 'Ok',
                    'data_enlace' => $data_enlace
                ], 200);
            }
            throw new Exception('Enlace no encontrado');
        } catch(Exception $e) {
            return response()->json([
                'status' => 'Error',
            ], 500);
        }
    }

    /**
     * Actualizar al enlace
     */
    public function updateEnlace(UpdateEnlace $request)
    {
        try {
            DB::connection('main')->beginTransaction();
            $funcionario_id = $request->enlaceId;
            $funcionario = DB::connection('main')
                ->table('osaf_funcionarios_cat')
                ->where('FuncionarioID', '=', $funcionario_id)
                ->first();
            //Si el correo es diferente del actual validar que no este repetido
            $data_funcionario = [];
            if($funcionario->Email != $request->email) {
                $exists_email = DB::connection('main')
                    ->table('osaf_funcionarios_cat')
                    ->where('Email', '=', strtolower($request->email))
                    ->whereNull('deleted_at')
                    ->first();
                if($exists_email) {
                    throw new Exception('El correo ya se encuentra registrado', 502);
                } else {
                    $data_funcionario['Email'] = strtolower($request->email);
                }
            }
            $data_funcionario['Nombres'] = mb_strtoupper($request->nombre, 'UTF-8');
            $data_funcionario['PrimerApellido'] = mb_strtoupper($request->primer_apellido, 'UTF-8');
            $data_funcionario['SegundoApellido'] = mb_strtoupper($request->segundo_apellido, 'UTF-8');
            $data_funcionario['Profesion'] = mb_strtoupper($request->profession, 'UTF-8');
            $data_funcionario['Telefono'] = $request->telefono;
            $data_funcionario['updated_at'] = date('Y-m-d H:i:s');
            //Actualizar al catalogo de funcionario
            DB::connection('main')
                ->table('osaf_funcionarios_cat')
                ->where('FuncionarioID', '=', $funcionario_id)
                ->update($data_funcionario);
            //Si el correo institucional es diferente del actual validar que no este repetido
            $funcionario_det = DB::connection('main')
                ->table('osaf_entidades_funcionarios_det')
                ->where('FuncionarioID', '=', $funcionario_id)
                ->first();
            //Si el correo es diferente del actual validar que no este repetido
            $data_funcionario_det = [];
            if($funcionario_det->Email != $request->correo_institucional) {
                $exists_email = DB::connection('main')
                    ->table('osaf_entidades_funcionarios_det')
                    ->where('Email', '=', strtolower($request->correo_institucional))
                    ->whereNull('deleted_at')
                    ->first();
                if($exists_email) {
                    throw new Exception('El correo institucional ya se encuentra registrado', 503);
                } else {
                    $correo_institucional = explode('@', $request->correo_institucional);
                    if($correo_institucional[1] == 'osaf.gob.mx'){
                        throw new Exception('No se puede registrar a un enlace con correo del OSAFIG', 501);
                    } else {
                        $data_funcionario_det['Email'] = strtolower($request->correo_institucional);
                    }
                }
            }
            $data_funcionario_det['FechaIngreso'] = $request->fecha_ingreso;
            $data_funcionario_det['Puesto'] = mb_strtoupper($request->puesto, 'UTF-8');
            $data_funcionario_det['updated_at'] = date('Y-m-d H:i:s');
            //Actualizar al catalogo de funcionario
            DB::connection('main')
                ->table('osaf_entidades_funcionarios_det')
                ->where('FuncionarioID', '=', $funcionario_id)
                ->update($data_funcionario_det);
            DB::connection('main')->commit();
            return response()->json([
                'status' => 'Ok'
            ], 200);
        } catch(Exception $e) {
            DB::connection('main')->rollBack();
            $code = ($e->getCode() != 500) ? $e->getCode() : 500;
            Log::error("ERROR: NO SE PUDO REGISTRAR AL NUEVO FUNCIONARIO | DETAIL: {$e->getMessage()}");
            return response()->json([
                'status'  => 'Error',
                'message' => 'No se pudo actualizar al enlace'
            ], $code);
        }
    }

    /**
     * Obtener los datos del enlace por token
     */
    public function getDataEnlaceByToken()
    {
        $enlace_det = DB::connection('main')
            ->table('osaf_entidades_funcionarios_det')
            ->where('api_token', '=', request()->api_token)
            ->first();
        $enlace_det->enlaceId = $enlace_det->FuncionarioID;
        $enlace = $this->enr->getEnlace($enlace_det);
        return response()->json([
            'status' => 'Ok',
            'enlace' => $enlace
        ], 200);
    }

    /**
     * Consultar si el enlace tiene una auiditoria en el ejercicio actual
     */
    public function checkHasAuditoria()
    {
        $ejercicio_fiscal = $this->er->getEjercicioAuditado()->Year;
        $auditoria = DB::table('v_control_auditorias')
            ->where('Ejercicio', '=', $ejercicio_fiscal)
            ->where('Entidad', '=', request()->Entidad)
            ->where('Enlace', '=', request()->FuncionarioID)
            ->first();
        if(is_null($auditoria)) {
            return response()->json([
                'status' => 'OK',
                'audControlInterno' => false
            ], 200);
        } else {
            return response()->json([
                'status' => 'OK',
                'audControlInterno' => true
            ], 200);
        }
    }
}
