<?php

namespace App\Http\Requests\ControlInterno;

use Illuminate\Foundation\Http\FormRequest;

class CreateEnlace extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = [
            "nombre"                => "required",
            "primer_apellido"       => "required",
            // "email"                 => "email",
            "entidad"               => "required|exists:main.osaf_entidades_fiscalizables,Nombre",
            "puesto"                => "required",
            "correo_institucional"  => "required|email|unique:main.osaf_v_funcionarios_det,Email",
        ];
        return $data;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'nombre.required'               => 'El Nombre es requerido.',
            'primer_apellido.required'      => 'El Primer apellido es requerido.',
            // 'email.email'                   => 'El Correo electrónico no es válido.',
            // 'email.unique'                  => 'El Correo electrónico ya se encuentra registrado',
            'entidad.required'              => 'La Entidad donde labora es requerida.',
            'entidad.exists'                => 'La Entidad donde labora no es válida',
            'puesto.required'               => 'El Cargo que desempeña es requerido.',
            'correo_institucional.required' => 'El Correo institucional es requerido.',
            'correo_institucional.email'    => 'El Correo institucional no es válido.',
            'correo_institucional.unique'   => 'El Correo institucional ya se encuentra registrado',
        ];
        return $messages;
    }
}
