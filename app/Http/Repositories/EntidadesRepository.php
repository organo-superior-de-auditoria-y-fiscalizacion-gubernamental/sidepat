<?php


namespace App\Http\Repositories;


use Illuminate\Support\Facades\DB;

class EntidadesRepository
{
    /**
     * Retornar la entidad por nombre
     * @param $name
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getEntidadByName($name)
    {
        return DB::connection('main')
            ->table('osaf_entidades_fiscalizables')
            ->where('Nombre', '=', $name)
            ->first();
    }
}
