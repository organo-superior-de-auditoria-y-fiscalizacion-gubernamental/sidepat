<?php


namespace App\Http\Repositories\Declaracion;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class DeclaracionRepository
{
    protected $db_declaracion;
    protected $db_main;

    public function __construct()
    {
        $this->db_declaracion = DB::connection('declaracion');
        $this->db_main        = DB::connection('main');
    }

    /**
     * Obtiene el ejercicio fiscal a declarar
     */
    public function getEjercicioDeclaracion()
    {
        $ejercicioActual = $this->db_main
            ->table('osaf_ejercicios_cat')
            ->where('Actual', '=', true)
            ->first();
        $ejercicioAnterior = (int) $ejercicioActual->EjercicioID - 1;
        return $this->db_main
            ->table('osaf_ejercicios_cat')
            ->where('EjercicioID', '=', $ejercicioAnterior)
            ->first();
    }

    /**
     * Obtener el tipo de declaración a presentar
     */
    public function getTipoDeclaracion($user, $ejercicioId)
    {
        $declaracion = $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales')
            ->where('UsuarioID', '=', $user)
            ->where('EjercicioID', '=', $ejercicioId)
            ->first();
        if(is_null($declaracion)) {
            return false;
        } else {
            $tipoDeclaracion = $this->db_declaracion
                ->table('oic_tipo_declaracion_cat')
                ->where('TipoDeclaracionID', '=', $declaracion->TipoDeclaracionID)
                ->first();
            if(is_null($tipoDeclaracion)) {
                return false;
            } else {
                return $tipoDeclaracion;
            }
        }
    }

    /**
     * Obtener los módulos a declarar
     */
    public function getSeccionesADeclarar($user, $ejercicioID)
    {
        return $this->db_declaracion->select('call sp_estadoDeclaracion(?, ?)', [
            $user,
            $ejercicioID
        ]);
    }

    /**
     * Obtener a los usuarios a declarar
     */
    public function getUsuariosDeclaracion($ejercicioID)
    {
        return $this->db_declaracion->select('call sp_declaracionesPorEjercicio(?)', [$ejercicioID]);
    }

    public function getTiposDeclaracion()
    {
        return $this->db_declaracion->table('oic_tipo_declaracion_cat')->get();
    }

    /**
     * Obtener declaración patrimonial por usuario
     */
    public function getDeclaracionByUser($user, $ejercicio)
    {
        return $this->db_declaracion->table('oic_declaraciones_patrimoniales')
            ->where('UsuarioID', '=', $user)
            ->where('EjercicioID', '=', $ejercicio)
            ->where('Eliminado', '=', 0)
            ->first();
    }

    /**
     * Obtener datos declaracion
     */
    public function getDeclaracionData($user,$ejercicioID){
        return $this->db_declaracion
            ->table('oic_declaraciones_patrimoniales')
            ->select('DeclaracionID','DeclaracionFecha','ObservacionesAclaraciones','RutaArchivoSat','Finalizado','DeclaracionLeida','Hash', 'DireccionID')
            ->where('UsuarioID','=', $user)
            ->where('Eliminado','=', "0")
            ->where('EjercicioID','=',$ejercicioID)
            ->get();
    }

    /**
     * Obtener datos personales
     */
    public function getDatosPersonalesMain($user){
        $datos = $this->db_main
            ->table('osaf_usuarios as db')
            ->select('db.UsuarioID','db.Nombres','db.PrimerApellido','db.SegundoApellido','db.CURP','db.RFC','db.Email',
                'db2.EmailPersonal', 'db2.Homoclave','db2.TelefonoParticular','db2.TelefonoCelular',
                'db2.EstadoCivilID', 'db2.PaisNacimiento','db2.Nacionalidad','db2.Observaciones', 'db2.RegimenMatrimonial',
                'db.PuestoID','db3.Nombre as Puesto')
            ->leftJoin('osafgobm_declaracion.oic_datos_personales as db2','db.UsuarioID', '=', 'db2.UsuarioMainID')
            ->join('osaf_puestos_cat as db3', 'db.PuestoID', '=', 'db3.PuestoID')
            ->where('db.UsuarioID', '=', $user)
            ->first();
        if(is_null($datos)){
            return false;
        } else{
            return $datos;
        }
    }

    /**
     * Obtener catalogo de estado civil
     */
    public function getCatalogoEstadoCivil(){
        $catalogo = $this->db_declaracion
            ->table('oic_estado_civil_cat')
            ->select('EstadoCivilID','EstadoCivil')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Guardar datos personales
     */
    public function registrarDatosPersonales($info){
        $result = $this->db_declaracion->select('call sp_registrarDatosPersonales(?,?,?,?,?,?,?,?,?,?,?)',[
            $info["UsuarioID"],
            $info["Homoclave"],
            $info["EmailPersonal"],
            $info["TelefonoParticular"],
            $info["TelefonoCelular"],
            $info["EstadoCivilID"],
            $info["RegimenMatrimonial"],
            $info["PaisNacimiento"],
            $info["Nacionalidad"],
            $info["Observaciones"],
            $info["declaracion_id"]
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Registrar la direccion proporcionada
     */
    public function registrarDireccion($request, $usuario)
    {
        if ($request["pais"] == null || $request['residencia'] == 0) {
            $pais = 'México';
            $ciudad = '';
            $municipio_id = $request["municipio_id"];
            $estado_id = $request["estado_id"];
            $estado_extranjero = null;
            $colonia = $request["colonia"];
        } else {
            $pais = $request["pais"];
            $ciudad = $request["ciudad"];
            $municipio_id = null;
            $estado_id = null;
            $estado_extranjero = $request["estado_extranjero"];
            $colonia = null;
        }
        $result = $this->db_declaracion->select('call sp_nuevaDireccion(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
            $request["residencia"],
            $request["calle"],
            $request["numero_exterior"],
            $request["numero_interior"],
            $colonia,
            $municipio_id,
            $estado_id,
            $request["codigo_postal"],
            $ciudad,
            $estado_extranjero,
            $pais,
            $request["observaciones"],
            $request["direccion_id"],
            $usuario,
        ]);
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }
    /*
     * Cargar catalogo de niveles de estudio
     */
    public function getCatalogoNivelesEstudio(){
        $catalogo = $this->db_declaracion
            ->table('oic_nivel_estudios_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        }else{
            return $catalogo;
        }
    }

    /**
     * Guardar datos curriculares
     */
    public function registrarDatosCurriculares($info,$user){
        $result = $this->db_declaracion->select('call sp_registrarDatosCurriculares(?,?,?,?,?,?,?,?,?,?)',[
            $info["nivel_estudio_id"],
            $info["institucion"],
            $info["carrera"],
            $info["status"],
            $info["documento"],
            $info["fecha_documento"],
            $info["es_extranjero"],
            $info["observaciones"],
            $info["declaracion_id"],
            $user
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Obtener estados
     */
    public function getEstados()
    {
        return $this->db_declaracion->table('oic_estados_cat')->get();
    }

    /**
     * Obtener municipios
     */
    public function getMunicipios($estado_id)
    {
        return $this->db_declaracion->table('oic_municipios_cat')
            ->where('EstadoID', '=', $estado_id)
            ->get();
    }

    public function getAreasAdscripcion()
    {
        return $this->db_declaracion->table('oic_adscripciones_cat')->get();
    }

    public function getNivelesCargo()
    {
        return $this->db_declaracion->table('oic_nivel_empleo_cat')->get();
    }

    /**
     * Obtener ordenes de gobierno
     */
    public function getOrdenesGobierno() {
        return $this->db_declaracion->table('oic_orden_gobierno')->get();
    }

    /**
     * Obtener ambitos publicos
     */
    public function getAmbitosPublicos() {
        return $this->db_declaracion->table('oic_ambito_cat')->get();
    }

    /**
     * Obtener direccion del osafig
     */
    public function getDireccionOsafig()
    {
        $data_conf = $this->db_declaracion->table('oic_configuraciones')
            ->where('ConfigID', '=', 1)
            ->first();
        return $this->db_declaracion->table('oic_direcciones as od')
            ->join('oic_estados_cat as oec', 'od.EstadoID', '=', 'oec.EstadoID')
            ->join('oic_municipios_cat as omc', 'od.MunicipioID', '=', 'omc.MunicipioID')
            ->where('od.DireccionID', '=', $data_conf->DireccionEnteID)
            ->select('od.*', 'oec.Nombre as Estado', 'omc.Nombre as Municipio')
            ->first();
    }

    /**
     * Guardar datos del cargo
     */
    public function registrarDatosCargo($request, $usuario)
    {
        $data = [
            $request['declaracion_id'],
            $usuario,
            $request['orden'],
            $request['ambito'],
            $request['ente'],
            $request['area'],
            $request['puesto_id'],
            $request['honorarios'],
            $request['nivel'],
            $request['funciones'],
            $request['fecha'],
            $request['telefono'],
            $request['direccion_id'],
            $request['aclaraciones'],
        ];
        $result = $this->db_declaracion->select('call sp_registrarCargo(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $data);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Obtener catalogo de orden de gobierno
     */
    public function getCatalogoOrdenGobierno(){
        $catalogo = $this->db_declaracion
            ->table('oic_orden_gobierno')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de ambito publico
     */
    public function getCatalogoAmbitoPublico(){
        $catalogo = $this->db_declaracion
            ->table('oic_ambito_cat')
            ->where('Activo', '=', 1)
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de sectores laborales
     */
    public function getCatalogoSectorLaboral(){
        $catalogo = $this->db_declaracion
            ->table('oic_sector_productivo_cat')
            ->select('SectorID','Sector')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Guardar experiencia laboral
     */
    public function registrarExpLaboral($info, $usuario){
        $sector = $this->db_declaracion
            ->table('oic_sector_productivo_cat')
            ->where('Sector', '=', $info['sector_id'])
            ->first();
        $sector_id = (is_null($sector)) ? 0 : $sector->SectorID;
        $result = $this->db_declaracion->select('call sp_registrarExperienciaLaboral (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[
            $usuario,
            $info['ambito_sector'],
            $info['orden_gobierno_id'],
            $info['ambito_id'],
            $info['institucion'],
            $info['rfc_institucion'],
            $info['area'],
            $info['puesto'],
            $info['funcion_principal'],
            $sector_id,
            $info['fecha_ingreso'],
            $info['fecha_egreso'],
            $info['en_extranjero'],
            $info['observacion'],
            $info['experiencia_laboral_id'],
            $info['otro_sector']
        ]);
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar experiencias laborales
     */
    public function getExperienciasLaborales($usuario){
        $result = $this->db_declaracion
            ->table('oic_experiencia_laboral_detalle')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->limit(5)
            ->orderBy('FechaTermino','desc')
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Marcar seccion como terminada
     */
    public function marcarSeccionTerminada($declaracion_id, $seccion)
    {
        return $this->db_declaracion
            ->table('oic_declaracion_estatus_det')
            ->where('DeclaracionID', '=', $declaracion_id)
            ->where('SeccionID', '=', $seccion)
            ->update([
                'EstadoActual' => true,
                'updated_at'   => date('Y-m-d H:i:s')
            ]);
    }

    /**
     * Cargar catalogo de instrumentos financieros
     */
    public function getCatalogoInstrumentos(){
        return $this->db_declaracion->table('oic_tipo_instrumento_cat')->get();
    }

    /**
     * Obtener ingresos de la declaracion
     */
    public function getIngresosDeclaracion($declaracion)
    {
        return $this->db_declaracion->table('oic_ingresos')
            ->where('DeclaracionID', '=', $declaracion)
            ->first();
    }

    /**
     * Obtener ingresos de la declaracion servidor pasado
     */
    public function getIngresosDeclaracionServidorPasado($declaracion)
    {
        return $this->db_declaracion->table('oic_servidor_pasado')
            ->where('DeclaracionID', '=', $declaracion)
            ->where('Oculto', '=', false)
            ->first();
    }

    /**
     * Obtener ingresos de la declaracion servidor pasado ocultos
     */
    public function getIngresosDeclaracionServidorPasadoOcultos($declaracion)
    {
        return $this->db_declaracion->table('oic_servidor_pasado')
            ->where('DeclaracionID', '=', $declaracion)
            ->where('Oculto', '=', true)
            ->get();
    }

    /**
     * Obtener ingresos por actividades industriales
     */
    public function getIngresosActividadesIndustriales($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_actividad_indutrial');
    }

    /**
     * Obtener ingresos por actividades financieras
     */
    public function getIngresosActividadesFinancieras($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_actividad_financiera');
    }

    /**
     * Obtener ingresos por servicios profesionales
     */
    public function getIngresosServiciosProfesionales($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_servicios');
    }

    /**
     * Obtener ingresos por otros ingresos
     */
    public function getIngresosOtros($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_extra');
    }

    public function getBienesEnajenados($ingreso)
    {
        return $this->getIngresosTipo($ingreso, 'oic_ingresos_por_enajenacion');
    }

    /**
     * Consulta el ingreso por tipo
     */
    public function getIngresosTipo($ingreso, $tipo)
    {
        return $this->db_declaracion
            ->table($tipo)
            ->where('IngresoID', '=', $ingreso)
            ->get();
    }

    /**
     * Obtener catalogo de bienes inmuebles
     */
    public function getCatalogoBienesInmueble(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Inmueble')
            ->orWhere('Categoria','=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de titulares
     */
    public function getCatalogoTipoTitulares(){
        $catalogo = $this->db_declaracion
            ->table('oic_titulares_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de vinculos personales
     */
    public function getCatalogoVinculoPersonal(){
        $catalogo = $this->db_declaracion
            ->table('oic_vinculo_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

     /**
     * Obtener catalogo de tipo de inversiones
     */
    public function getCatalogoTipoInversion(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_inversion_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de adquisicion
     */
    public function getCatalogoTipoAdquisicion(){
        $catalogo = $this->db_declaracion
            ->table('oic_forma_operacion_cat')
            ->where('Activo', '=', '1')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipos de vehiculos
     */
    public function getCatalogoVehiculos(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Vehículo')
            ->orWhere('Categoria', '=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Cargar bienes inmuebles
     */
    public function getBienesInmueble($usuario)
    {
        $result = $this->db_declaracion
            ->table('oic_bienes_inmubles')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Cargar vehiculos
     */
    public function getVehiculos($usuario){
        $result = $this->db_declaracion
            ->table('oic_bienes_muebles as obm')
            ->select('obm.BienMuebleID', 'obm.DeclaracionID', 'obm.TipoBienID', 'obm.Titular',
                'obm.TipoPersonaTransmisor', 'obm.NombreRazonSocial', 'obm.RFCTRansmisor',
                'obm.RelacionTransmisor', 'obm.NombreTercero', 'obm.TipoPersonaTercero',
                'obm.RFCTercero', 'obm.FormaAdquisicionID', 'obm.FormaPago',
                'obm.Valor', 'obm.TipoMoneda', 'obm.FechaOperacion',
                'obm.Caracteristicas', 'obm.VehiculoMarca', 'obm.VehiculoModelo', 'obm.VehiculoNumeroSerie',
                'obm.VehiculoAnio', 'obm.MotivoBaja', 'obm.DescripcionBien',
                'obm.Ubicacion', 'obm.OtroTipoBien', 'obm.UsuarioID',
                'otbc.Categoria', 'obm.Observaciones')
            ->join('oic_tipo_bien_cat as otbc','obm.TipoBienID', '=','otbc.TipoBienID')
            ->where('otbc.Categoria', '=', 'Vehiculo')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Obtener catalogo de tipos de bienes mueble
     */
    public function getCatalogoBienesMueble(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_bien_cat')
            ->where('Categoria', '=', 'Mueble')
            ->orWhere('Categoria', '=', 'Todas')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

     /**
     * Obtener catalogo de activos
     */
    public function getCatalogoActivos($tipo){
        $catalogo = $this->db_declaracion
            ->table('oic_activos_financieros')
            ->where('TipoInversionID', '=', $tipo)
             ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Obtener catalogo de tipo de adeudos
     */
    public function getCatalogoTipoAdeudo(){
        $catalogo = $this->db_declaracion
            ->table('oic_tipo_adeudo_cat')
            ->get();
        if(is_null($catalogo)){
            return false;
        } else{
            return $catalogo;
        }
    }

    /**
     * Cargar bienes mueble
     */
    public function getBienesMueble($usuario){
        $result = $this->db_declaracion
            ->table('oic_bienes_muebles as obm')
            ->select('obm.BienMuebleID', 'obm.DeclaracionID', 'obm.TipoBienID', 'obm.Titular',
                'obm.TipoPersonaTransmisor', 'obm.NombreRazonSocial', 'obm.RFCTRansmisor',
                'obm.RelacionTransmisor', 'obm.NombreTercero', 'obm.TipoPersonaTercero',
                'obm.RFCTercero', 'obm.FormaAdquisicionID', 'obm.FormaPago',
                'obm.Valor', 'obm.TipoMoneda', 'obm.FechaOperacion',
                'obm.Caracteristicas', 'otbc.Categoria',
                'obm.Ubicacion', 'obm.OtroTipoBien', 'obm.UsuarioID', 'obm.MotivoBaja', 'obm.Observaciones', 'obm.DescripcionBien')
            ->join('oic_tipo_bien_cat as otbc','obm.TipoBienID', '=','otbc.TipoBienID')
            ->where('otbc.Categoria', '=', 'Mueble')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

     /**
     * Cargar inversiones
     */
    public function getInversiones($usuario){
        $result = $this->db_declaracion
            ->table('oic_recursos_financieros')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar catalogo tipos participante
     */
    public function getCatalogoTipoParticipante(){
        $result = $this->db_declaracion
            ->table('oic_tipo_participante')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }
    /**
     * cargar catalogo de tipo institucion
     */
    public function getCatalogoTipoInstitucion(){
        $result = $this->db_declaracion
            ->table('oic_tipo_institucion')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar inversiones
     */
    public function getAdeudos($usuario){
        $result = $this->db_declaracion
            ->table('oic_adeudos')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar clientes
     */
    public function getClientes($usuario){
        $result = $this->db_declaracion
            ->table('oic_clientes_principales')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar participacion institucion
     */
    public function getParticipacionInstitucion($usuario)
    {
        $result = $this->db_declaracion
            ->table('oic_participacion_instituciones')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * cargar catalogo de beneficiarios
     */
    public function getCatalogoBeneficiarios(){
        $result = $this->db_declaracion
            ->table('oic_beneficiarios_cat')
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar participacion empresas
     */
    public function getParticipacionEmpresas($usuario){
        $result = $this->db_declaracion
            ->table('oic_participacion_empresas')
            ->select('ParticipacionID', 'NombreEmpresa', 'RFCEmpresa', 'PorcentajeParticipacion', 'TipoParticipacion',
                'RecibeRemuneracion', 'MontoMensual', 'Ubicacion', 'EsExtranjero', 'Participante',
                'SectorID', 'Observaciones', 'DeclaracionID', 'created_at', 'updated_at', 'usuarioID', 'Oculto')
                ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    public function getRepresentaciones($usuario){
        $result = $this->db_declaracion
            ->table('oic_representacion')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar apoyos y beneficios
     */
    public function getApoyosBeneficios($usuario){
        $result = $this->db_declaracion
            ->table('oic_apoyos_beneficios')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar clientes
     */
    public function getBeneficiosPrivados($usuario){
        $result = $this->db_declaracion
            ->table('oic_beneficios_privados')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }

    /**
     * Cargar prestamos
     */
    public function getPrestamos($usuario)
    {
        $result = $this->db_declaracion
            ->table('oic_prestamo_comodato')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if (is_null($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Cargar inversiones
     */
    public function getFideicomisos($usuario){
        $result = $this->db_declaracion
            ->table('oic_fideicomisos')
            ->where('UsuarioID', '=', $usuario)
            ->where('Oculto', '=', false)
            ->get();
        if(is_null($result)){
            return false;
        } else{
            return $result;
        }
    }
}
