<?php


namespace App\Http\Repositories\AdministracionTi;

use Illuminate\Support\Facades\DB;


class UserRepository
{
    protected $main_db;
    protected $accesos_db;

    public function __construct()
    {
        $this->main_db    = DB::connection('main');
        $this->accesos_db = DB::connection('accesos');
    }

    /**
     * Obtener roles de usuario
     */
    public function getRoles()
    {
        return $this->accesos_db->table('osaf_tipo_usuario_cat as otuc')
            ->join('osaf_plataforma_cat as opc', 'otuc.PlataformaID', '=', 'opc.PlataformaID')
            ->select('otuc.*', 'opc.Descripcion as Plataforma')
            ->get();
    }
}
