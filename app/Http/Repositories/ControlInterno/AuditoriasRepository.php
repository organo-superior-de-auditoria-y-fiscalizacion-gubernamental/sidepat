<?php

namespace App\Http\Repositories\ControlInterno;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\{Crypt, DB, Hash, Log};
use App\Http\Repositories\EtapasRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuditoriasRepository
{
    protected $etr; //Repositorio de etapas de Auditorias

    public function __construct(EtapasRepository $etr)
    {
        $this->etr = $etr;
    }
    /**
     * Retornar las auditorias creadas por ejercicio
     * @param $EjercicioID
     * @return \Illuminate\Support\Collection
     */
    public function getAuditoriasCreadas($EjercicioID)
    {
        return DB::table('control_auditorias')
            ->where('EjercicioID', '=', $EjercicioID->EjercicioID)
            ->get();
    }

    /**
     * Retorna al jefe de auditoria de desempeño
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getJefeAuditoria()
    {
        return DB::connection('main')
            ->table('osaf_v_jefe_auditoria_desempenio')
            ->select('*')
            ->first();
    }

    /**
     * Retorna a los auditores de desempeño
     * @return \Illuminate\Support\Collection
     */
    public function getAuditores()
    {
        return DB::connection('main')
            ->table('osaf_v_auditores_desempenio')
            ->select('*')
            ->get();
    }

    /**
     * Retornar al auditor asignado a la auditoria
     * @param $auditoria
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getAuditorAsignado($auditoria)
    {
        $asignacion_personal_auditor = DB::table('control_asignacion_personal_auditoria')
            ->where('AuditoriaID', '=', $auditoria)
            ->where('Enlace', '=', 0)
            ->where('ResponsableAuditoria', '', 0)
            ->whereNull('deleted_at')
            ->first();
        if(is_null($asignacion_personal_auditor)) {
            return '';
        }
        return DB::connection('main')
            ->table('osaf_v_auditores_desempenio')
            ->where('UsuarioID', '=', $asignacion_personal_auditor->UsuarioID)
            ->first();
    }

    /**
     * Retornar al enlace asignado a la auditoria
     * @param $auditoria
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|string|null
     */
    public function getEnlaceAsignado($auditoria)
    {
        $asignacion_personal_auditor = DB::table('control_asignacion_personal_auditoria')
            ->where('AuditoriaID', '=', $auditoria)
            ->where('Enlace', '=', 1)
            ->where('ResponsableAuditoria', '', 0)
            ->whereNull('deleted_at')
            ->first();
        if(is_null($asignacion_personal_auditor)) {
            return '';
        }
        return DB::connection('main')
            ->table('osaf_v_enlaces')
            ->where('FuncionarioID', '=', $asignacion_personal_auditor->UsuarioID)
            ->select('*')
            ->first();
    }

    /**
     * Retornar el enlace por id o nombre
     * @param null $id
     * @param null $nombre
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getEnlace($id = null, $nombre = null, $auditoria_id = null)
    {
        if(!is_null($id)) {
            return DB::connection('main')
                ->table('osaf_v_enlaces')
                ->where('FuncionarioID', '=', $id)
                ->first();
        }
        if(!is_null($nombre) && is_null($auditoria_id)) {
            return DB::connection('main')
                ->table('osaf_v_enlaces')
                ->where('NombreCompleto', '=', $nombre)
                ->first();
        }
        if(!is_null($nombre) && !is_null($auditoria_id)) {
            $auditoria = DB::table('v_control_auditorias')
                ->where('AuditoriaID', '=', $auditoria_id)
                ->first();
            return DB::connection('main')
                ->table('osaf_v_enlaces')
                ->where('NombreCompleto', '=', $nombre)
                ->where('Entidad', '=', $auditoria->Entidad)
                ->first();
        }
    }

    /**
     * Obtener los enlaces ligados a un ente en particular
     * @param $auditoria
     * @return array
     */
    public function getEnlacesEnte($auditoria)
    {
        return DB::select('call sp_obtenerEnlacesuditoria(?)', [$auditoria]);
    }

    /**
     * Obtener los datos del usuario enlace para el formato de accesos o correo electronico
     * @param $request
     */
    public function getDataAccessosPdf($request)
    {
        //Obtener nombre de enlace, nombre de ente y cargo del ente
        $data = $this->getEnlace(null, $request->enlace, $request->auditoria);
        try {
            Crypt::decryptString($data->Password);
        } catch (DecryptException $e) {
            if($e->getMessage() == 'The MAC is invalid.' || $e->getMessage() == 'The payload is invalid.') {
                try {
                    DB::connection('main')->beginTransaction();
                    $str = strtoupper(Str::random(8));
                    $str_encrypt = Crypt::encryptString($str);
                    DB::connection('main')
                        ->table('osaf_entidades_funcionarios_det')
                        ->where('FuncionarioID', '=', $data->FuncionarioID)
                        ->update([
                            'Password' => Hash::make($str),
                            'PasswordDecrypted' => $str_encrypt,
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    DB::connection('main')->commit();
                    $data = $this->getEnlace(null, $request->enlace);
                }catch(\Exception $e) {
                    DB::connection('main')->rollBack();
                    Log::error('ERROR | NO SE PUDO RESETAR LA CONTRASEÑA');
                    $data = false;
                }
            } else {
                Log::error('ERROR | NO SE PUDO DESENCRIPTAR LA CONTRASEÑA');
                $data = false;
            }
        }
        return $data;
    }

    /**
     * Obtener las prorrogas de los enlaces
     * @param $auditoria
     * @return array|\Illuminate\Support\Collection
     */
    public function getProrrogas($auditoria)
    {
        //Obtener las etapas de los enlaces
        $etapas_enlace = $this->etr->getEtapasEnlace();
        $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
        //Obtener los detalles de etapa auditoria
        $etapa_auditoria_det = DB::table('control_auditoria_etapa_det')
            ->where('AuditoriaID', '=', $auditoria)
            ->whereIn('EtapaID', $etapas_id)
            ->get();
        $auditorias_etapa_id = Arr::pluck($etapa_auditoria_det, 'AuditoriaEtapaID');
        //Obtener las prorrogas de las etapas del enlace
        $prorrogas = DB::table('control_prorrogas_cat')
            ->whereIn('AuditoriaEtapaID', $auditorias_etapa_id)
            ->orderBy('AuditoriaEtapaID', 'asc')
            ->get();
        if(count($prorrogas) > 0){
            return $prorrogas;
        }
        return [];
    }

    /**
     * Retornar el detalle de la auditoria
     * @param $auditoria
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getAuditoriaDetalle($auditoria)
    {
        return DB::table('v_control_auditorias')
            ->where('AuditoriaID', '=', $auditoria)
            ->first();
    }

    /**
     * Retornar las fechas para aplicar cuestionarios de la auditoria
     */
    public function getAuditoriaFechas($auditoria)
    {
        $etapas_enlace = $this->etr->getEtapasEnlace();
        $etapas_id = Arr::pluck($etapas_enlace, 'EtapaID');
        // Obtener los registros del detalle de auditoria etapas
        $det_auditoria_etapas = DB::table('control_auditoria_etapa_det')
            ->whereIn('EtapaID', $etapas_id)
            ->where('AuditoriaID', '=', $auditoria)
            ->select('*')
            ->get();
        // Consultar fechas de inicio y fin de etapas
        $fechas = [];
        for($i = 0; $i < count($det_auditoria_etapas); $i++) {
            $fecha = DB::table('control_fechas_etapa_det')
                ->where('AuditoriaEtapaID', $det_auditoria_etapas[$i]->AuditoriaEtapaID)
                ->select('*')
                ->first();
            if(is_null($fecha)) {
                array_push($fechas, [
                    'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                    'fecha_inicial' => null,
                    'fecha_final' => null
                ]);
            } else {
                array_push($fechas, [
                    'etapa' => $det_auditoria_etapas[$i]->Secuencial,
                    'fecha_inicial' => $fecha->FechaInicio,
                    'fecha_final' => $fecha->FechaFin
                ]);
            }
        }
        return $fechas;
    }

    /**
     * Obtener respuestas de auditoria de control interno
     */
    public function getCountRespuestas($etapaId, $userId, $ejercicioId)
    {
        return DB::table('v_estatus_auditoria')
            ->where('EtapaActual', '=', $etapaId)
            ->where('UsuarioActual', '=', $userId)
            ->where('EjercicioID', '=', $ejercicioId)
            ->count();
    }
}
