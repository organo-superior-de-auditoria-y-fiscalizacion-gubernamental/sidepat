<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class AuthenticateEnlace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('api_token')) {
            //Validar que el token sea de un usuario enlace
            $funcionario_det = DB::connection('main')
                ->table('osaf_entidades_funcionarios_det')
                ->where('api_token', '=', $request->api_token)
                ->first();
            if(!is_null($funcionario_det)) {
                return $next($request);
            } else {
                return response()->json([
                    'status' => 'Error | usuario no encontrado'
                ], 500);
            }
        } else {
            return response()->json([
                'status' => 'Error | token no encontrado'
            ], 500);
        }
    }
}
